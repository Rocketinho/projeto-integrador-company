﻿using Company_seguros.zTelas._1Usuario;
using Company_seguros.zTelas._4Compra;
using Company_seguros.zTelas._6Fornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MENU());
        }
    }
}
