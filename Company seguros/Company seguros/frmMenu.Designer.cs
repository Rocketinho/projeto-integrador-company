﻿namespace Company_seguros
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuUsuario = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalvarU = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuBuscarU = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalvarC = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarC = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalvarV = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultaProdutoV = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuNovaV = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarV = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCOmpra = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuNovoP = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarP = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuCadastrarCo = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarCompra = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFornecedor = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuCadastrarF = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarF = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFinanceiro = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarFC = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRH = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuBaterR = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuFolha = new System.Windows.Forms.ToolStripMenuItem();
            this.label17 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.LightGray;
            this.statusStrip1.Location = new System.Drawing.Point(0, 299);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(544, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuUsuario,
            this.menuCliente,
            this.menuVenda,
            this.menuCOmpra,
            this.menuEstoque,
            this.menuFornecedor,
            this.menuFinanceiro,
            this.menuRH});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(544, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuUsuario
            // 
            this.menuUsuario.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalvarU,
            this.subMenuBuscarU});
            this.menuUsuario.Name = "menuUsuario";
            this.menuUsuario.Size = new System.Drawing.Size(59, 20);
            this.menuUsuario.Text = "Usuário";
            this.menuUsuario.Click += new System.EventHandler(this.menuUsuario_Click);
            // 
            // subMenuSalvarU
            // 
            this.subMenuSalvarU.Name = "subMenuSalvarU";
            this.subMenuSalvarU.Size = new System.Drawing.Size(180, 22);
            this.subMenuSalvarU.Text = "Cadastrar";
            this.subMenuSalvarU.Click += new System.EventHandler(this.subMenuSalvarU_Click);
            // 
            // subMenuBuscarU
            // 
            this.subMenuBuscarU.Name = "subMenuBuscarU";
            this.subMenuBuscarU.Size = new System.Drawing.Size(180, 22);
            this.subMenuBuscarU.Text = "Buscar";
            this.subMenuBuscarU.Click += new System.EventHandler(this.subMenuBuscarU_Click);
            // 
            // menuCliente
            // 
            this.menuCliente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalvarC,
            this.subMenuConsultarC});
            this.menuCliente.Name = "menuCliente";
            this.menuCliente.Size = new System.Drawing.Size(56, 20);
            this.menuCliente.Text = "Cliente";
            // 
            // subMenuSalvarC
            // 
            this.subMenuSalvarC.Name = "subMenuSalvarC";
            this.subMenuSalvarC.Size = new System.Drawing.Size(180, 22);
            this.subMenuSalvarC.Text = "Cadastrar";
            this.subMenuSalvarC.Click += new System.EventHandler(this.cadrastrarToolStripMenuItem_Click);
            // 
            // subMenuConsultarC
            // 
            this.subMenuConsultarC.Name = "subMenuConsultarC";
            this.subMenuConsultarC.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarC.Text = "Buscar";
            this.subMenuConsultarC.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // menuVenda
            // 
            this.menuVenda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalvarV,
            this.subMenuConsultaProdutoV,
            this.subMenuNovaV,
            this.subMenuConsultarV});
            this.menuVenda.Name = "menuVenda";
            this.menuVenda.Size = new System.Drawing.Size(51, 20);
            this.menuVenda.Text = "Venda";
            // 
            // subMenuSalvarV
            // 
            this.subMenuSalvarV.Name = "subMenuSalvarV";
            this.subMenuSalvarV.Size = new System.Drawing.Size(180, 22);
            this.subMenuSalvarV.Text = "Novo Serviço";
            this.subMenuSalvarV.Click += new System.EventHandler(this.produtosParaVendaToolStripMenuItem_Click);
            // 
            // subMenuConsultaProdutoV
            // 
            this.subMenuConsultaProdutoV.Name = "subMenuConsultaProdutoV";
            this.subMenuConsultaProdutoV.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultaProdutoV.Text = "Consultar Serviços";
            this.subMenuConsultaProdutoV.Click += new System.EventHandler(this.consultarProdutoToolStripMenuItem_Click);
            // 
            // subMenuNovaV
            // 
            this.subMenuNovaV.Name = "subMenuNovaV";
            this.subMenuNovaV.Size = new System.Drawing.Size(180, 22);
            this.subMenuNovaV.Text = "Nova Venda";
            this.subMenuNovaV.Click += new System.EventHandler(this.novaVendaToolStripMenuItem_Click);
            // 
            // subMenuConsultarV
            // 
            this.subMenuConsultarV.Name = "subMenuConsultarV";
            this.subMenuConsultarV.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarV.Text = "Consultar vendas";
            this.subMenuConsultarV.Click += new System.EventHandler(this.consultarVendasToolStripMenuItem_Click);
            // 
            // menuCOmpra
            // 
            this.menuCOmpra.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuNovoP,
            this.subMenuConsultarP,
            this.subMenuCadastrarCo,
            this.subMenuConsultarCompra});
            this.menuCOmpra.Name = "menuCOmpra";
            this.menuCOmpra.Size = new System.Drawing.Size(62, 20);
            this.menuCOmpra.Text = "Compra";
            this.menuCOmpra.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // subMenuNovoP
            // 
            this.subMenuNovoP.Name = "subMenuNovoP";
            this.subMenuNovoP.Size = new System.Drawing.Size(180, 22);
            this.subMenuNovoP.Text = "Novo Produto";
            this.subMenuNovoP.Click += new System.EventHandler(this.novoProdutoToolStripMenuItem_Click);
            // 
            // subMenuConsultarP
            // 
            this.subMenuConsultarP.Name = "subMenuConsultarP";
            this.subMenuConsultarP.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarP.Text = "Consultar Produto";
            this.subMenuConsultarP.Click += new System.EventHandler(this.subMenuConsultarCo_Click);
            // 
            // subMenuCadastrarCo
            // 
            this.subMenuCadastrarCo.Name = "subMenuCadastrarCo";
            this.subMenuCadastrarCo.Size = new System.Drawing.Size(180, 22);
            this.subMenuCadastrarCo.Text = "Nova Compra";
            this.subMenuCadastrarCo.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // subMenuConsultarCompra
            // 
            this.subMenuConsultarCompra.Name = "subMenuConsultarCompra";
            this.subMenuConsultarCompra.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarCompra.Text = "Consultar Compra";
            this.subMenuConsultarCompra.Click += new System.EventHandler(this.consultarCompraToolStripMenuItem_Click);
            // 
            // menuEstoque
            // 
            this.menuEstoque.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem});
            this.menuEstoque.Name = "menuEstoque";
            this.menuEstoque.Size = new System.Drawing.Size(61, 20);
            this.menuEstoque.Text = "Estoque";
            this.menuEstoque.Click += new System.EventHandler(this.estoqueToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.consultarToolStripMenuItem.Text = "Consultar ";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // menuFornecedor
            // 
            this.menuFornecedor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuCadastrarF,
            this.subMenuConsultarF});
            this.menuFornecedor.Name = "menuFornecedor";
            this.menuFornecedor.Size = new System.Drawing.Size(79, 20);
            this.menuFornecedor.Text = "Fornecedor";
            // 
            // subMenuCadastrarF
            // 
            this.subMenuCadastrarF.Name = "subMenuCadastrarF";
            this.subMenuCadastrarF.Size = new System.Drawing.Size(180, 22);
            this.subMenuCadastrarF.Text = "Cadastrar";
            this.subMenuCadastrarF.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // subMenuConsultarF
            // 
            this.subMenuConsultarF.Name = "subMenuConsultarF";
            this.subMenuConsultarF.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarF.Text = "Buscar";
            this.subMenuConsultarF.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // menuFinanceiro
            // 
            this.menuFinanceiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuConsultarFC});
            this.menuFinanceiro.Name = "menuFinanceiro";
            this.menuFinanceiro.Size = new System.Drawing.Size(74, 20);
            this.menuFinanceiro.Text = "Financeiro";
            // 
            // subMenuConsultarFC
            // 
            this.subMenuConsultarFC.Name = "subMenuConsultarFC";
            this.subMenuConsultarFC.Size = new System.Drawing.Size(180, 22);
            this.subMenuConsultarFC.Text = "Fluxo de Caixa";
            this.subMenuConsultarFC.Click += new System.EventHandler(this.produtoParaCopToolStripMenuItem_Click);
            // 
            // menuRH
            // 
            this.menuRH.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuBaterR,
            this.subMenuFolha});
            this.menuRH.Name = "menuRH";
            this.menuRH.Size = new System.Drawing.Size(35, 20);
            this.menuRH.Text = "RH";
            // 
            // subMenuBaterR
            // 
            this.subMenuBaterR.Name = "subMenuBaterR";
            this.subMenuBaterR.Size = new System.Drawing.Size(183, 22);
            this.subMenuBaterR.Text = "Bater Ponto";
            this.subMenuBaterR.Click += new System.EventHandler(this.baterPontoToolStripMenuItem_Click);
            // 
            // subMenuFolha
            // 
            this.subMenuFolha.Name = "subMenuFolha";
            this.subMenuFolha.Size = new System.Drawing.Size(183, 22);
            this.subMenuFolha.Text = "Folha de pagamento";
            this.subMenuFolha.Click += new System.EventHandler(this.folhaDePagamentoToolStripMenuItem_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.LightGray;
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(521, 2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 20);
            this.label17.TabIndex = 41;
            this.label17.Text = "X";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(544, 321);
            this.ControlBox = false;
            this.Controls.Add(this.label17);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMenu_FormClosing);
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuUsuario;
        private System.Windows.Forms.ToolStripMenuItem subMenuBuscarU;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalvarU;
        private System.Windows.Forms.ToolStripMenuItem menuCliente;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarC;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalvarC;
        private System.Windows.Forms.ToolStripMenuItem menuVenda;
        private System.Windows.Forms.ToolStripMenuItem subMenuNovaV;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarV;
        private System.Windows.Forms.ToolStripMenuItem menuEstoque;
        private System.Windows.Forms.ToolStripMenuItem menuFornecedor;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarF;
        private System.Windows.Forms.ToolStripMenuItem subMenuCadastrarF;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalvarV;
        private System.Windows.Forms.ToolStripMenuItem menuCOmpra;
        private System.Windows.Forms.ToolStripMenuItem subMenuCadastrarCo;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarP;
        private System.Windows.Forms.ToolStripMenuItem menuFinanceiro;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarFC;
        private System.Windows.Forms.ToolStripMenuItem menuRH;
        private System.Windows.Forms.ToolStripMenuItem subMenuBaterR;
        private System.Windows.Forms.ToolStripMenuItem subMenuFolha;
        private System.Windows.Forms.ToolStripMenuItem subMenuNovoP;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultaProdutoV;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarCompra;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.Label label17;
    }
}