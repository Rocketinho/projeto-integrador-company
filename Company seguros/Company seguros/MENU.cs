﻿using Company_seguros.Ferramentas;
using Company_seguros.zTelas._1Usuario;
using Company_seguros.zTelas._2Cliente;
using Company_seguros.zTelas._3Venda;
using Company_seguros.zTelas._4Compra;
using Company_seguros.zTelas._5Estoque;
using Company_seguros.zTelas._6Fornecedor;
using Company_seguros.zTelas._8RH;
using Company_seguros.zTelas.Cliente;
using Company_seguros.zTelas.Fluxo_de_caixa;
using Company_seguros.zTelas.Módulo_2.Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros
{
    public partial class MENU : Form
    {
        public MENU()
        {
            InitializeComponent();
            SidePanel.Height = btnUsuario.Height;
            panelUsuario.BringToFront();
            
            //CarregarPermissoes();
            Atual = this;
        }

        //private void AbrirForm(object form)
        //{
        //    if (this.panelPrincipal.Controls.Count > 0)
        //        this.panelPrincipal.Controls.RemoveAt(0);
        //    Form fh = form as Form;
        //    fh.TopLevel = false;
        //    fh.Dock = DockStyle.Fill;
        //    this.panelPrincipal.Controls.Add(fh);
        //    this.panelPrincipal.Tag = fh;
        //    fh.Show();
        //}

        public static MENU Atual;



        //private void CarregarPermissoes()
        //{
        //    if (UserSession.Logado.Salvar == false)
        //    {
        //        subMenuSalvarU.Enabled = false;
        //        subMenuSalvarC.Enabled = false;
        //        subMenuNovaV.Enabled = false;
        //        subMenuCadastrarCo.Enabled = false;
        //        subMenuCadastrarF.Enabled = false;
        //        subMenuSalvarV.Enabled = false;
        //        subMenuNovoP.Enabled = false;


        //    }
        //    if (UserSession.Logado.Consultar == false)
        //    {
        //        subMenuBuscarU.Enabled = false;
        //        subMenuConsultarC.Enabled = false;
        //        subMenuConsultarV.Enabled = false;
        //        subMenuConsultarP.Enabled = false;
        //        subMenuConsultaProdutoV.Enabled = false;
        //        menuEstoque.Enabled = false;
        //        subMenuConsultarF.Enabled = false;
        //        subMenuConsultarFC.Enabled = false;
        //        subMenuFolha.Enabled = false;
        //        subMenuConsultarCompra.Enabled = false;
        //    }

        //}

        private void MENU_Load(object sender, EventArgs e)
        {

        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnUsuario.Height;
            SidePanel.Top = btnUsuario.Top;

            panelUsuario.BringToFront();

        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnCliente.Height;
            SidePanel.Top = btnCliente.Top;
            panelCliente.BringToFront();

        }

        private void btnVendas_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnVendas.Height;
            SidePanel.Top = btnVendas.Top;
            panelVenda.BringToFront();
        }

        private void btnCompra_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnCompra.Height;
            SidePanel.Top = btnCompra.Top;
            panelCompra.BringToFront();
        }

        private void btnEstoque_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnEstoque.Height;
            SidePanel.Top = btnEstoque.Top;
            panelEstoque.BringToFront();
        }

        private void btnFornecedor_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnFornecedor.Height;
            SidePanel.Top = btnFornecedor.Top;
            panelFornecedor.BringToFront();
        }

        private void btnRH_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnRH.Height;
            SidePanel.Top = btnRH.Top;
            panelRH.BringToFront();
        }

        private void btnFinanceiro_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnFinanceiro.Height;

            SidePanel.Top = btnFinanceiro.Top;
            panelFinanceiro.BringToFront();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnUsuario2_Click(object sender, EventArgs e)
        {
            UsuarioCadastrar frm = new UsuarioCadastrar();
            frm.Show();
            Hide();
        }

        private void btnUsuario3_Click(object sender, EventArgs e)
        {
            UsuarioBuscar frm = new UsuarioBuscar();
            frm.Show();
            Hide();
        }

        private void SidePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 170)
            {
                while(MenuVertical.Width >= 42)
                {
                    MenuVertical.Width = MenuVertical.Width - 1;
                }
            }
            else
            {
                MenuVertical.Width = 170;
            }
                
        }

        private void panelPrincipal_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MenuVertical_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            UsuarioCadastrar form = new UsuarioCadastrar();
            form.Show();
            Hide();
        }

        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            UsuarioBuscar frm = new UsuarioBuscar();
            frm.Show();
            Hide();

        }

        private void bunifuTileButton2_Click_1(object sender, EventArgs e)
        {
            UsuarioBuscar frm = new UsuarioBuscar();
            frm.Show();
            Hide();
        }

        private void bunifuTileButton4_Click(object sender, EventArgs e)
        {
            CadastrarProdutoVenda frm = new CadastrarProdutoVenda();
            frm.Show();
            Hide();

        }

        private void bunifuTileButton3_Click(object sender, EventArgs e)
        {
            Buscar_ProdutosVenda frm = new Buscar_ProdutosVenda();
            frm.Show();
            Hide();
        }

        private void subMenuSalvarC_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente frm = new Cadastrar_Cliente();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarC_Click(object sender, EventArgs e)
        {
            BuscarCliente frm = new BuscarCliente();
            frm.Show();
            Hide();
        }

        private void subMenuNovaV_Click(object sender, EventArgs e)
        {
            Novavenda frm = new Novavenda();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarV_Click(object sender, EventArgs e)
        {
            ProdutosVenda frm = new ProdutosVenda();
            frm.Show();
            Hide();
        }

        private void subMenuNovoP_Click(object sender, EventArgs e)
        {
            CadastrarProdutoCompra frm = new CadastrarProdutoCompra();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarP_Click(object sender, EventArgs e)
        {
            ProdutosCompra frm = new ProdutosCompra();
            frm.Show();
            Hide();
        }

        private void subMenuCadastrarCo_Click(object sender, EventArgs e)
        {
            NovaCompra frm = new NovaCompra();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarCompra_Click(object sender, EventArgs e)
        {
            ConsultarCompra frm = new ConsultarCompra();
            frm.Show();
            Hide();
        }

        private void subMenuCadastrarF_Click(object sender, EventArgs e)
        {
            CadastrarFornecedor frm = new CadastrarFornecedor();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarF_Click(object sender, EventArgs e)
        {
            BuscarFornecedor frm = new BuscarFornecedor();
            frm.Show();
            Hide();
        }

        private void menuEstoque_Click(object sender, EventArgs e)
        {
            
        }

        private void menuEstoque_Click_1(object sender, EventArgs e)
        {
            ConsultarEstoque frm = new ConsultarEstoque();
            frm.Show();
            Hide();
        }

        private void subMenuConsultarFC_Click(object sender, EventArgs e)
        {
            Fluxo_de_Caixa frm = new Fluxo_de_Caixa();
            frm.Show();
            Hide();
        }

        private void subMenuBaterR_Click(object sender, EventArgs e)
        {
            BaterPonto frm = new BaterPonto();
            frm.Show();
            Hide();
        }

        private void subMenuFolha_Click(object sender, EventArgs e)
        {
            FolhaPagamento frm = new FolhaPagamento();
            frm.Show();
            Hide();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panelUsuario_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelCliente_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelVenda_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelCompra_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelEstoque_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelFinanceiro_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelRH_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelFornecedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
