﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.Ferramentas
{
    public class Nulos
    {
        public string VerificarRG(string rg)
        {
            if(rg == "  ,   ,   -")
            {
                return rg = null;
            }
            else
            {
                return rg;
            }
        }

        public string VerificarCPF(string cpf)
        {
            if (cpf == "         /")
            {
                return cpf = null;
            }
            else
            {
                return cpf;
            }
        }

        public string VerificarCNPJ(string cnpj)
        {
            if (cnpj == "   ,   ,   /    -")
            {
                return cnpj = string.Empty;
            }
            else
            {
                return cnpj = "";
            }
        }

    }
}
