﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.Ferramentas.Localizacao
{
   public class Localizacao
    {
        public List<string> UF()
        {
            List<string> uf = new List<string>();
         
            uf.Add("AC");
            uf.Add("AL");
            uf.Add("AM");
            uf.Add("AP");
            uf.Add("BA");
            uf.Add("CE");
            uf.Add("DF");
            uf.Add("ES");
            uf.Add("GO");
            uf.Add("MA");
            uf.Add("MG");
            uf.Add("MS");
            uf.Add("MT");
            uf.Add("PA");
            uf.Add("PB");
            uf.Add("PE");
            uf.Add("PI");
            uf.Add("PR");
            uf.Add("RJ");
            uf.Add("RN");
            uf.Add("RO");
            uf.Add("RR");
            uf.Add("RS");
            uf.Add("SC");
            uf.Add("SE");
            uf.Add("SP");
            uf.Add("TO");

            return uf;
        }
    }
}
