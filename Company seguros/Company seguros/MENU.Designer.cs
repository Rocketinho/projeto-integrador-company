﻿namespace Company_seguros
{
    partial class MENU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MENU));
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.SidePanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnUsuario = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.btnFinanceiro = new System.Windows.Forms.Button();
            this.btnVendas = new System.Windows.Forms.Button();
            this.btnRH = new System.Windows.Forms.Button();
            this.btnCompra = new System.Windows.Forms.Button();
            this.btnFornecedor = new System.Windows.Forms.Button();
            this.btnEstoque = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panelUsuario = new System.Windows.Forms.Panel();
            this.subMenuBuscarU = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuSalvarU = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelCliente = new System.Windows.Forms.Panel();
            this.subMenuConsultarC = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuSalvarC = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelVenda = new System.Windows.Forms.Panel();
            this.subMenuConsultarV = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuNovaV = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuConsultaProdutoV = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuSalvarV = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.panelRH = new System.Windows.Forms.Panel();
            this.subMenuFolha = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuBaterR = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelFornecedor = new System.Windows.Forms.Panel();
            this.subMenuConsultarF = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuCadastrarF = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelEstoque = new System.Windows.Forms.Panel();
            this.menuEstoque = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelCompra = new System.Windows.Forms.Panel();
            this.subMenuConsultarCompra = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuCadastrarCo = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuConsultarP = new Bunifu.Framework.UI.BunifuTileButton();
            this.subMenuNovoP = new Bunifu.Framework.UI.BunifuTileButton();
            this.panelFinanceiro = new System.Windows.Forms.Panel();
            this.subMenuConsultarFC = new Bunifu.Framework.UI.BunifuTileButton();
            this.MenuVertical.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelUsuario.SuspendLayout();
            this.panelCliente.SuspendLayout();
            this.panelVenda.SuspendLayout();
            this.panelPrincipal.SuspendLayout();
            this.panelRH.SuspendLayout();
            this.panelFornecedor.SuspendLayout();
            this.panelEstoque.SuspendLayout();
            this.panelCompra.SuspendLayout();
            this.panelFinanceiro.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.MenuVertical.Controls.Add(this.SidePanel);
            this.MenuVertical.Controls.Add(this.panel2);
            this.MenuVertical.Controls.Add(this.btnUsuario);
            this.MenuVertical.Controls.Add(this.btnCliente);
            this.MenuVertical.Controls.Add(this.btnFinanceiro);
            this.MenuVertical.Controls.Add(this.btnVendas);
            this.MenuVertical.Controls.Add(this.btnRH);
            this.MenuVertical.Controls.Add(this.btnCompra);
            this.MenuVertical.Controls.Add(this.btnFornecedor);
            this.MenuVertical.Controls.Add(this.btnEstoque);
            this.MenuVertical.Cursor = System.Windows.Forms.Cursors.Default;
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 0);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(170, 686);
            this.MenuVertical.TabIndex = 17;
            this.MenuVertical.Paint += new System.Windows.Forms.PaintEventHandler(this.MenuVertical_Paint);
            // 
            // SidePanel
            // 
            this.SidePanel.BackColor = System.Drawing.Color.SteelBlue;
            this.SidePanel.Location = new System.Drawing.Point(1, 149);
            this.SidePanel.Name = "SidePanel";
            this.SidePanel.Size = new System.Drawing.Size(5, 46);
            this.SidePanel.TabIndex = 17;
            this.SidePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.SidePanel_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(36, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(98, 117);
            this.panel2.TabIndex = 22;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(24, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Seguros";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Company ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Company_seguros.Properties.Resources.wifi_connection_signal_symbol;
            this.pictureBox1.Location = new System.Drawing.Point(9, -9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnUsuario
            // 
            this.btnUsuario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUsuario.FlatAppearance.BorderSize = 0;
            this.btnUsuario.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsuario.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsuario.ForeColor = System.Drawing.Color.White;
            this.btnUsuario.Image = global::Company_seguros.Properties.Resources.monitor__3_;
            this.btnUsuario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUsuario.Location = new System.Drawing.Point(4, 149);
            this.btnUsuario.Name = "btnUsuario";
            this.btnUsuario.Size = new System.Drawing.Size(165, 46);
            this.btnUsuario.TabIndex = 9;
            this.btnUsuario.Text = "        Usuário";
            this.btnUsuario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUsuario.UseVisualStyleBackColor = true;
            this.btnUsuario.Click += new System.EventHandler(this.btnUsuario_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCliente.FlatAppearance.BorderSize = 0;
            this.btnCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.ForeColor = System.Drawing.Color.White;
            this.btnCliente.Image = global::Company_seguros.Properties.Resources.customer__1_;
            this.btnCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCliente.Location = new System.Drawing.Point(4, 201);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(165, 46);
            this.btnCliente.TabIndex = 10;
            this.btnCliente.Text = "        Cliente";
            this.btnCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // btnFinanceiro
            // 
            this.btnFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinanceiro.FlatAppearance.BorderSize = 0;
            this.btnFinanceiro.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnFinanceiro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinanceiro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinanceiro.ForeColor = System.Drawing.Color.White;
            this.btnFinanceiro.Image = global::Company_seguros.Properties.Resources.debt;
            this.btnFinanceiro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFinanceiro.Location = new System.Drawing.Point(5, 461);
            this.btnFinanceiro.Name = "btnFinanceiro";
            this.btnFinanceiro.Size = new System.Drawing.Size(165, 46);
            this.btnFinanceiro.TabIndex = 16;
            this.btnFinanceiro.Text = "        Financeiro";
            this.btnFinanceiro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFinanceiro.UseVisualStyleBackColor = true;
            this.btnFinanceiro.Click += new System.EventHandler(this.btnFinanceiro_Click);
            // 
            // btnVendas
            // 
            this.btnVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVendas.FlatAppearance.BorderSize = 0;
            this.btnVendas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnVendas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVendas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVendas.ForeColor = System.Drawing.Color.White;
            this.btnVendas.Image = global::Company_seguros.Properties.Resources.investment;
            this.btnVendas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVendas.Location = new System.Drawing.Point(4, 253);
            this.btnVendas.Name = "btnVendas";
            this.btnVendas.Size = new System.Drawing.Size(165, 46);
            this.btnVendas.TabIndex = 11;
            this.btnVendas.Text = "        Vendas";
            this.btnVendas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVendas.UseVisualStyleBackColor = true;
            this.btnVendas.Click += new System.EventHandler(this.btnVendas_Click);
            // 
            // btnRH
            // 
            this.btnRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRH.FlatAppearance.BorderSize = 0;
            this.btnRH.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnRH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRH.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRH.ForeColor = System.Drawing.Color.White;
            this.btnRH.Image = global::Company_seguros.Properties.Resources.resume;
            this.btnRH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRH.Location = new System.Drawing.Point(5, 513);
            this.btnRH.Name = "btnRH";
            this.btnRH.Size = new System.Drawing.Size(165, 46);
            this.btnRH.TabIndex = 15;
            this.btnRH.Text = "        RH";
            this.btnRH.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRH.UseVisualStyleBackColor = true;
            this.btnRH.Click += new System.EventHandler(this.btnRH_Click);
            // 
            // btnCompra
            // 
            this.btnCompra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCompra.FlatAppearance.BorderSize = 0;
            this.btnCompra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompra.ForeColor = System.Drawing.Color.White;
            this.btnCompra.Image = global::Company_seguros.Properties.Resources.shopping_cart__1_;
            this.btnCompra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCompra.Location = new System.Drawing.Point(4, 305);
            this.btnCompra.Name = "btnCompra";
            this.btnCompra.Size = new System.Drawing.Size(165, 46);
            this.btnCompra.TabIndex = 12;
            this.btnCompra.Text = "       Compras";
            this.btnCompra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCompra.UseVisualStyleBackColor = true;
            this.btnCompra.Click += new System.EventHandler(this.btnCompra_Click);
            // 
            // btnFornecedor
            // 
            this.btnFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFornecedor.FlatAppearance.BorderSize = 0;
            this.btnFornecedor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFornecedor.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFornecedor.ForeColor = System.Drawing.Color.White;
            this.btnFornecedor.Image = global::Company_seguros.Properties.Resources.delivery_truck;
            this.btnFornecedor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFornecedor.Location = new System.Drawing.Point(4, 409);
            this.btnFornecedor.Name = "btnFornecedor";
            this.btnFornecedor.Size = new System.Drawing.Size(165, 46);
            this.btnFornecedor.TabIndex = 14;
            this.btnFornecedor.Text = "        Fornecedor";
            this.btnFornecedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFornecedor.UseVisualStyleBackColor = true;
            this.btnFornecedor.Click += new System.EventHandler(this.btnFornecedor_Click);
            // 
            // btnEstoque
            // 
            this.btnEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEstoque.FlatAppearance.BorderSize = 0;
            this.btnEstoque.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstoque.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstoque.ForeColor = System.Drawing.Color.White;
            this.btnEstoque.Image = global::Company_seguros.Properties.Resources.package;
            this.btnEstoque.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstoque.Location = new System.Drawing.Point(4, 357);
            this.btnEstoque.Name = "btnEstoque";
            this.btnEstoque.Size = new System.Drawing.Size(165, 46);
            this.btnEstoque.TabIndex = 13;
            this.btnEstoque.Text = "        Estoque";
            this.btnEstoque.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEstoque.UseVisualStyleBackColor = true;
            this.btnEstoque.Click += new System.EventHandler(this.btnEstoque_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(170, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(700, 46);
            this.panel4.TabIndex = 21;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(662, 9);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 23);
            this.label17.TabIndex = 42;
            this.label17.Text = "X";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(3, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 26);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 20;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panelUsuario
            // 
            this.panelUsuario.Controls.Add(this.subMenuBuscarU);
            this.panelUsuario.Controls.Add(this.subMenuSalvarU);
            this.panelUsuario.Location = new System.Drawing.Point(185, 189);
            this.panelUsuario.Name = "panelUsuario";
            this.panelUsuario.Size = new System.Drawing.Size(319, 285);
            this.panelUsuario.TabIndex = 0;
            this.panelUsuario.Paint += new System.Windows.Forms.PaintEventHandler(this.panelUsuario_Paint);
            // 
            // subMenuBuscarU
            // 
            this.subMenuBuscarU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuBuscarU.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuBuscarU.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuBuscarU.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuBuscarU.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuBuscarU.ForeColor = System.Drawing.Color.White;
            this.subMenuBuscarU.Image = ((System.Drawing.Image)(resources.GetObject("subMenuBuscarU.Image")));
            this.subMenuBuscarU.ImagePosition = 14;
            this.subMenuBuscarU.ImageZoom = 50;
            this.subMenuBuscarU.LabelPosition = 29;
            this.subMenuBuscarU.LabelText = "Consultar Usuario";
            this.subMenuBuscarU.Location = new System.Drawing.Point(178, 90);
            this.subMenuBuscarU.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuBuscarU.Name = "subMenuBuscarU";
            this.subMenuBuscarU.Size = new System.Drawing.Size(123, 112);
            this.subMenuBuscarU.TabIndex = 4;
            this.subMenuBuscarU.Click += new System.EventHandler(this.bunifuTileButton2_Click_1);
            // 
            // subMenuSalvarU
            // 
            this.subMenuSalvarU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarU.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarU.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuSalvarU.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuSalvarU.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuSalvarU.ForeColor = System.Drawing.Color.White;
            this.subMenuSalvarU.Image = ((System.Drawing.Image)(resources.GetObject("subMenuSalvarU.Image")));
            this.subMenuSalvarU.ImagePosition = 14;
            this.subMenuSalvarU.ImageZoom = 50;
            this.subMenuSalvarU.LabelPosition = 29;
            this.subMenuSalvarU.LabelText = "Cadastrar Usuario";
            this.subMenuSalvarU.Location = new System.Drawing.Point(27, 90);
            this.subMenuSalvarU.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuSalvarU.Name = "subMenuSalvarU";
            this.subMenuSalvarU.Size = new System.Drawing.Size(123, 112);
            this.subMenuSalvarU.TabIndex = 3;
            this.subMenuSalvarU.Click += new System.EventHandler(this.bunifuTileButton1_Click);
            // 
            // panelCliente
            // 
            this.panelCliente.Controls.Add(this.subMenuConsultarC);
            this.panelCliente.Controls.Add(this.subMenuSalvarC);
            this.panelCliente.Location = new System.Drawing.Point(185, 189);
            this.panelCliente.Name = "panelCliente";
            this.panelCliente.Size = new System.Drawing.Size(319, 285);
            this.panelCliente.TabIndex = 5;
            this.panelCliente.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCliente_Paint);
            // 
            // subMenuConsultarC
            // 
            this.subMenuConsultarC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarC.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarC.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarC.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarC.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarC.Image")));
            this.subMenuConsultarC.ImagePosition = 14;
            this.subMenuConsultarC.ImageZoom = 50;
            this.subMenuConsultarC.LabelPosition = 29;
            this.subMenuConsultarC.LabelText = "Consultar Cliente";
            this.subMenuConsultarC.Location = new System.Drawing.Point(178, 90);
            this.subMenuConsultarC.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarC.Name = "subMenuConsultarC";
            this.subMenuConsultarC.Size = new System.Drawing.Size(123, 112);
            this.subMenuConsultarC.TabIndex = 4;
            this.subMenuConsultarC.Click += new System.EventHandler(this.subMenuConsultarC_Click);
            // 
            // subMenuSalvarC
            // 
            this.subMenuSalvarC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarC.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarC.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuSalvarC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuSalvarC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuSalvarC.ForeColor = System.Drawing.Color.White;
            this.subMenuSalvarC.Image = ((System.Drawing.Image)(resources.GetObject("subMenuSalvarC.Image")));
            this.subMenuSalvarC.ImagePosition = 14;
            this.subMenuSalvarC.ImageZoom = 50;
            this.subMenuSalvarC.LabelPosition = 29;
            this.subMenuSalvarC.LabelText = "Cadastrar Cliente";
            this.subMenuSalvarC.Location = new System.Drawing.Point(27, 90);
            this.subMenuSalvarC.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuSalvarC.Name = "subMenuSalvarC";
            this.subMenuSalvarC.Size = new System.Drawing.Size(123, 112);
            this.subMenuSalvarC.TabIndex = 3;
            this.subMenuSalvarC.Click += new System.EventHandler(this.subMenuSalvarC_Click);
            // 
            // panelVenda
            // 
            this.panelVenda.Controls.Add(this.subMenuConsultarV);
            this.panelVenda.Controls.Add(this.subMenuNovaV);
            this.panelVenda.Controls.Add(this.subMenuConsultaProdutoV);
            this.panelVenda.Controls.Add(this.subMenuSalvarV);
            this.panelVenda.Location = new System.Drawing.Point(185, 189);
            this.panelVenda.Name = "panelVenda";
            this.panelVenda.Size = new System.Drawing.Size(319, 285);
            this.panelVenda.TabIndex = 5;
            this.panelVenda.Paint += new System.Windows.Forms.PaintEventHandler(this.panelVenda_Paint);
            // 
            // subMenuConsultarV
            // 
            this.subMenuConsultarV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarV.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarV.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarV.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarV.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarV.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarV.Image")));
            this.subMenuConsultarV.ImagePosition = 14;
            this.subMenuConsultarV.ImageZoom = 50;
            this.subMenuConsultarV.LabelPosition = 29;
            this.subMenuConsultarV.LabelText = "Consultar Venda";
            this.subMenuConsultarV.Location = new System.Drawing.Point(162, 153);
            this.subMenuConsultarV.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarV.Name = "subMenuConsultarV";
            this.subMenuConsultarV.Size = new System.Drawing.Size(131, 101);
            this.subMenuConsultarV.TabIndex = 8;
            this.subMenuConsultarV.Click += new System.EventHandler(this.subMenuConsultarV_Click);
            // 
            // subMenuNovaV
            // 
            this.subMenuNovaV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuNovaV.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuNovaV.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuNovaV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuNovaV.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuNovaV.ForeColor = System.Drawing.Color.White;
            this.subMenuNovaV.Image = ((System.Drawing.Image)(resources.GetObject("subMenuNovaV.Image")));
            this.subMenuNovaV.ImagePosition = 14;
            this.subMenuNovaV.ImageZoom = 50;
            this.subMenuNovaV.LabelPosition = 29;
            this.subMenuNovaV.LabelText = "Nova Venda";
            this.subMenuNovaV.Location = new System.Drawing.Point(20, 153);
            this.subMenuNovaV.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuNovaV.Name = "subMenuNovaV";
            this.subMenuNovaV.Size = new System.Drawing.Size(131, 101);
            this.subMenuNovaV.TabIndex = 7;
            this.subMenuNovaV.Click += new System.EventHandler(this.subMenuNovaV_Click);
            // 
            // subMenuConsultaProdutoV
            // 
            this.subMenuConsultaProdutoV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultaProdutoV.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultaProdutoV.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultaProdutoV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultaProdutoV.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultaProdutoV.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultaProdutoV.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultaProdutoV.Image")));
            this.subMenuConsultaProdutoV.ImagePosition = 14;
            this.subMenuConsultaProdutoV.ImageZoom = 50;
            this.subMenuConsultaProdutoV.LabelPosition = 29;
            this.subMenuConsultaProdutoV.LabelText = "Consultar Serviço";
            this.subMenuConsultaProdutoV.Location = new System.Drawing.Point(162, 30);
            this.subMenuConsultaProdutoV.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultaProdutoV.Name = "subMenuConsultaProdutoV";
            this.subMenuConsultaProdutoV.Size = new System.Drawing.Size(131, 101);
            this.subMenuConsultaProdutoV.TabIndex = 6;
            this.subMenuConsultaProdutoV.Click += new System.EventHandler(this.bunifuTileButton3_Click);
            // 
            // subMenuSalvarV
            // 
            this.subMenuSalvarV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarV.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuSalvarV.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuSalvarV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuSalvarV.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuSalvarV.ForeColor = System.Drawing.Color.White;
            this.subMenuSalvarV.Image = ((System.Drawing.Image)(resources.GetObject("subMenuSalvarV.Image")));
            this.subMenuSalvarV.ImagePosition = 14;
            this.subMenuSalvarV.ImageZoom = 50;
            this.subMenuSalvarV.LabelPosition = 29;
            this.subMenuSalvarV.LabelText = "Novo Serviço";
            this.subMenuSalvarV.Location = new System.Drawing.Point(20, 30);
            this.subMenuSalvarV.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuSalvarV.Name = "subMenuSalvarV";
            this.subMenuSalvarV.Size = new System.Drawing.Size(131, 101);
            this.subMenuSalvarV.TabIndex = 5;
            this.subMenuSalvarV.Click += new System.EventHandler(this.bunifuTileButton4_Click);
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.Controls.Add(this.panelVenda);
            this.panelPrincipal.Controls.Add(this.panelRH);
            this.panelPrincipal.Controls.Add(this.panelCliente);
            this.panelPrincipal.Controls.Add(this.panelUsuario);
            this.panelPrincipal.Controls.Add(this.panelFornecedor);
            this.panelPrincipal.Controls.Add(this.panelEstoque);
            this.panelPrincipal.Controls.Add(this.panelCompra);
            this.panelPrincipal.Controls.Add(this.panelFinanceiro);
            this.panelPrincipal.Location = new System.Drawing.Point(170, 46);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(700, 640);
            this.panelPrincipal.TabIndex = 22;
            this.panelPrincipal.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPrincipal_Paint);
            // 
            // panelRH
            // 
            this.panelRH.Controls.Add(this.subMenuFolha);
            this.panelRH.Controls.Add(this.subMenuBaterR);
            this.panelRH.Location = new System.Drawing.Point(185, 189);
            this.panelRH.Name = "panelRH";
            this.panelRH.Size = new System.Drawing.Size(319, 283);
            this.panelRH.TabIndex = 8;
            this.panelRH.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRH_Paint);
            // 
            // subMenuFolha
            // 
            this.subMenuFolha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuFolha.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuFolha.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuFolha.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuFolha.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuFolha.ForeColor = System.Drawing.Color.White;
            this.subMenuFolha.Image = ((System.Drawing.Image)(resources.GetObject("subMenuFolha.Image")));
            this.subMenuFolha.ImagePosition = 14;
            this.subMenuFolha.ImageZoom = 50;
            this.subMenuFolha.LabelPosition = 29;
            this.subMenuFolha.LabelText = "Folha de Pagamento";
            this.subMenuFolha.Location = new System.Drawing.Point(153, 90);
            this.subMenuFolha.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuFolha.Name = "subMenuFolha";
            this.subMenuFolha.Size = new System.Drawing.Size(154, 112);
            this.subMenuFolha.TabIndex = 4;
            this.subMenuFolha.Click += new System.EventHandler(this.subMenuFolha_Click);
            // 
            // subMenuBaterR
            // 
            this.subMenuBaterR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuBaterR.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuBaterR.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuBaterR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuBaterR.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuBaterR.ForeColor = System.Drawing.Color.White;
            this.subMenuBaterR.Image = ((System.Drawing.Image)(resources.GetObject("subMenuBaterR.Image")));
            this.subMenuBaterR.ImagePosition = 14;
            this.subMenuBaterR.ImageZoom = 50;
            this.subMenuBaterR.LabelPosition = 29;
            this.subMenuBaterR.LabelText = "Bater Ponto";
            this.subMenuBaterR.Location = new System.Drawing.Point(20, 90);
            this.subMenuBaterR.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuBaterR.Name = "subMenuBaterR";
            this.subMenuBaterR.Size = new System.Drawing.Size(123, 112);
            this.subMenuBaterR.TabIndex = 3;
            this.subMenuBaterR.Click += new System.EventHandler(this.subMenuBaterR_Click);
            // 
            // panelFornecedor
            // 
            this.panelFornecedor.Controls.Add(this.subMenuConsultarF);
            this.panelFornecedor.Controls.Add(this.subMenuCadastrarF);
            this.panelFornecedor.Location = new System.Drawing.Point(185, 189);
            this.panelFornecedor.Name = "panelFornecedor";
            this.panelFornecedor.Size = new System.Drawing.Size(319, 285);
            this.panelFornecedor.TabIndex = 8;
            this.panelFornecedor.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFornecedor_Paint);
            // 
            // subMenuConsultarF
            // 
            this.subMenuConsultarF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarF.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarF.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarF.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarF.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarF.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarF.Image")));
            this.subMenuConsultarF.ImagePosition = 14;
            this.subMenuConsultarF.ImageZoom = 50;
            this.subMenuConsultarF.LabelPosition = 29;
            this.subMenuConsultarF.LabelText = "Consultar Fornecedor";
            this.subMenuConsultarF.Location = new System.Drawing.Point(170, 92);
            this.subMenuConsultarF.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarF.Name = "subMenuConsultarF";
            this.subMenuConsultarF.Size = new System.Drawing.Size(152, 112);
            this.subMenuConsultarF.TabIndex = 4;
            this.subMenuConsultarF.Click += new System.EventHandler(this.subMenuConsultarF_Click);
            // 
            // subMenuCadastrarF
            // 
            this.subMenuCadastrarF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuCadastrarF.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuCadastrarF.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuCadastrarF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuCadastrarF.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuCadastrarF.ForeColor = System.Drawing.Color.White;
            this.subMenuCadastrarF.Image = ((System.Drawing.Image)(resources.GetObject("subMenuCadastrarF.Image")));
            this.subMenuCadastrarF.ImagePosition = 14;
            this.subMenuCadastrarF.ImageZoom = 50;
            this.subMenuCadastrarF.LabelPosition = 29;
            this.subMenuCadastrarF.LabelText = "Cadastrar Fornecedor";
            this.subMenuCadastrarF.Location = new System.Drawing.Point(5, 92);
            this.subMenuCadastrarF.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuCadastrarF.Name = "subMenuCadastrarF";
            this.subMenuCadastrarF.Size = new System.Drawing.Size(159, 112);
            this.subMenuCadastrarF.TabIndex = 3;
            this.subMenuCadastrarF.Click += new System.EventHandler(this.subMenuCadastrarF_Click);
            // 
            // panelEstoque
            // 
            this.panelEstoque.Controls.Add(this.menuEstoque);
            this.panelEstoque.Location = new System.Drawing.Point(185, 189);
            this.panelEstoque.Name = "panelEstoque";
            this.panelEstoque.Size = new System.Drawing.Size(319, 285);
            this.panelEstoque.TabIndex = 9;
            this.panelEstoque.Paint += new System.Windows.Forms.PaintEventHandler(this.panelEstoque_Paint);
            // 
            // menuEstoque
            // 
            this.menuEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.menuEstoque.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.menuEstoque.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.menuEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuEstoque.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuEstoque.ForeColor = System.Drawing.Color.White;
            this.menuEstoque.Image = ((System.Drawing.Image)(resources.GetObject("menuEstoque.Image")));
            this.menuEstoque.ImagePosition = 14;
            this.menuEstoque.ImageZoom = 50;
            this.menuEstoque.LabelPosition = 29;
            this.menuEstoque.LabelText = "Consultar Estoque";
            this.menuEstoque.Location = new System.Drawing.Point(98, 90);
            this.menuEstoque.Margin = new System.Windows.Forms.Padding(5);
            this.menuEstoque.Name = "menuEstoque";
            this.menuEstoque.Size = new System.Drawing.Size(131, 101);
            this.menuEstoque.TabIndex = 7;
            this.menuEstoque.Click += new System.EventHandler(this.menuEstoque_Click_1);
            // 
            // panelCompra
            // 
            this.panelCompra.Controls.Add(this.subMenuConsultarCompra);
            this.panelCompra.Controls.Add(this.subMenuCadastrarCo);
            this.panelCompra.Controls.Add(this.subMenuConsultarP);
            this.panelCompra.Controls.Add(this.subMenuNovoP);
            this.panelCompra.Location = new System.Drawing.Point(185, 189);
            this.panelCompra.Name = "panelCompra";
            this.panelCompra.Size = new System.Drawing.Size(319, 285);
            this.panelCompra.TabIndex = 6;
            this.panelCompra.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCompra_Paint);
            // 
            // subMenuConsultarCompra
            // 
            this.subMenuConsultarCompra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarCompra.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarCompra.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarCompra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarCompra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarCompra.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarCompra.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarCompra.Image")));
            this.subMenuConsultarCompra.ImagePosition = 14;
            this.subMenuConsultarCompra.ImageZoom = 50;
            this.subMenuConsultarCompra.LabelPosition = 29;
            this.subMenuConsultarCompra.LabelText = "Consultar compra";
            this.subMenuConsultarCompra.Location = new System.Drawing.Point(162, 153);
            this.subMenuConsultarCompra.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarCompra.Name = "subMenuConsultarCompra";
            this.subMenuConsultarCompra.Size = new System.Drawing.Size(131, 101);
            this.subMenuConsultarCompra.TabIndex = 8;
            this.subMenuConsultarCompra.Click += new System.EventHandler(this.subMenuConsultarCompra_Click);
            // 
            // subMenuCadastrarCo
            // 
            this.subMenuCadastrarCo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuCadastrarCo.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuCadastrarCo.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuCadastrarCo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuCadastrarCo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuCadastrarCo.ForeColor = System.Drawing.Color.White;
            this.subMenuCadastrarCo.Image = ((System.Drawing.Image)(resources.GetObject("subMenuCadastrarCo.Image")));
            this.subMenuCadastrarCo.ImagePosition = 14;
            this.subMenuCadastrarCo.ImageZoom = 50;
            this.subMenuCadastrarCo.LabelPosition = 29;
            this.subMenuCadastrarCo.LabelText = "Nova compra";
            this.subMenuCadastrarCo.Location = new System.Drawing.Point(20, 153);
            this.subMenuCadastrarCo.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuCadastrarCo.Name = "subMenuCadastrarCo";
            this.subMenuCadastrarCo.Size = new System.Drawing.Size(131, 101);
            this.subMenuCadastrarCo.TabIndex = 7;
            this.subMenuCadastrarCo.Click += new System.EventHandler(this.subMenuCadastrarCo_Click);
            // 
            // subMenuConsultarP
            // 
            this.subMenuConsultarP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarP.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarP.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarP.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarP.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarP.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarP.Image")));
            this.subMenuConsultarP.ImagePosition = 14;
            this.subMenuConsultarP.ImageZoom = 50;
            this.subMenuConsultarP.LabelPosition = 29;
            this.subMenuConsultarP.LabelText = "Consultar Produto";
            this.subMenuConsultarP.Location = new System.Drawing.Point(162, 30);
            this.subMenuConsultarP.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarP.Name = "subMenuConsultarP";
            this.subMenuConsultarP.Size = new System.Drawing.Size(131, 101);
            this.subMenuConsultarP.TabIndex = 6;
            this.subMenuConsultarP.Click += new System.EventHandler(this.subMenuConsultarP_Click);
            // 
            // subMenuNovoP
            // 
            this.subMenuNovoP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuNovoP.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuNovoP.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuNovoP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuNovoP.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuNovoP.ForeColor = System.Drawing.Color.White;
            this.subMenuNovoP.Image = ((System.Drawing.Image)(resources.GetObject("subMenuNovoP.Image")));
            this.subMenuNovoP.ImagePosition = 14;
            this.subMenuNovoP.ImageZoom = 50;
            this.subMenuNovoP.LabelPosition = 29;
            this.subMenuNovoP.LabelText = "Novo Produto";
            this.subMenuNovoP.Location = new System.Drawing.Point(20, 30);
            this.subMenuNovoP.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuNovoP.Name = "subMenuNovoP";
            this.subMenuNovoP.Size = new System.Drawing.Size(131, 101);
            this.subMenuNovoP.TabIndex = 5;
            this.subMenuNovoP.Click += new System.EventHandler(this.subMenuNovoP_Click);
            // 
            // panelFinanceiro
            // 
            this.panelFinanceiro.Controls.Add(this.subMenuConsultarFC);
            this.panelFinanceiro.Location = new System.Drawing.Point(185, 189);
            this.panelFinanceiro.Name = "panelFinanceiro";
            this.panelFinanceiro.Size = new System.Drawing.Size(319, 282);
            this.panelFinanceiro.TabIndex = 10;
            this.panelFinanceiro.Paint += new System.Windows.Forms.PaintEventHandler(this.panelFinanceiro_Paint);
            // 
            // subMenuConsultarFC
            // 
            this.subMenuConsultarFC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarFC.color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.subMenuConsultarFC.colorActive = System.Drawing.Color.MediumSeaGreen;
            this.subMenuConsultarFC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.subMenuConsultarFC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subMenuConsultarFC.ForeColor = System.Drawing.Color.White;
            this.subMenuConsultarFC.Image = ((System.Drawing.Image)(resources.GetObject("subMenuConsultarFC.Image")));
            this.subMenuConsultarFC.ImagePosition = 14;
            this.subMenuConsultarFC.ImageZoom = 50;
            this.subMenuConsultarFC.LabelPosition = 29;
            this.subMenuConsultarFC.LabelText = "Fluxo de Caixa";
            this.subMenuConsultarFC.Location = new System.Drawing.Point(98, 90);
            this.subMenuConsultarFC.Margin = new System.Windows.Forms.Padding(5);
            this.subMenuConsultarFC.Name = "subMenuConsultarFC";
            this.subMenuConsultarFC.Size = new System.Drawing.Size(131, 101);
            this.subMenuConsultarFC.TabIndex = 7;
            this.subMenuConsultarFC.Click += new System.EventHandler(this.subMenuConsultarFC_Click);
            // 
            // MENU
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(870, 686);
            this.Controls.Add(this.panelPrincipal);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.MenuVertical);
            this.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MENU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MENU";
            this.Load += new System.EventHandler(this.MENU_Load);
            this.MenuVertical.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelUsuario.ResumeLayout(false);
            this.panelCliente.ResumeLayout(false);
            this.panelVenda.ResumeLayout(false);
            this.panelPrincipal.ResumeLayout(false);
            this.panelRH.ResumeLayout(false);
            this.panelFornecedor.ResumeLayout(false);
            this.panelEstoque.ResumeLayout(false);
            this.panelCompra.ResumeLayout(false);
            this.panelFinanceiro.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnFinanceiro;
        private System.Windows.Forms.Button btnRH;
        private System.Windows.Forms.Button btnFornecedor;
        private System.Windows.Forms.Button btnEstoque;
        private System.Windows.Forms.Button btnCompra;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button btnUsuario;
        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Panel SidePanel;
        private System.Windows.Forms.Button btnVendas;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.Panel panelCompra;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarCompra;
        private Bunifu.Framework.UI.BunifuTileButton subMenuCadastrarCo;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarP;
        private Bunifu.Framework.UI.BunifuTileButton subMenuNovoP;
        private System.Windows.Forms.Panel panelVenda;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarV;
        private Bunifu.Framework.UI.BunifuTileButton subMenuNovaV;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultaProdutoV;
        private Bunifu.Framework.UI.BunifuTileButton subMenuSalvarV;
        private System.Windows.Forms.Panel panelCliente;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarC;
        private Bunifu.Framework.UI.BunifuTileButton subMenuSalvarC;
        private System.Windows.Forms.Panel panelUsuario;
        private Bunifu.Framework.UI.BunifuTileButton subMenuBuscarU;
        private Bunifu.Framework.UI.BunifuTileButton subMenuSalvarU;
        private System.Windows.Forms.Panel panelEstoque;
        private Bunifu.Framework.UI.BunifuTileButton menuEstoque;
        private System.Windows.Forms.Panel panelFinanceiro;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarFC;
        private System.Windows.Forms.Panel panelRH;
        private Bunifu.Framework.UI.BunifuTileButton subMenuFolha;
        private Bunifu.Framework.UI.BunifuTileButton subMenuBaterR;
        private System.Windows.Forms.Panel panelFornecedor;
        private Bunifu.Framework.UI.BunifuTileButton subMenuConsultarF;
        private Bunifu.Framework.UI.BunifuTileButton subMenuCadastrarF;
        private System.Windows.Forms.Label label17;
    }
}