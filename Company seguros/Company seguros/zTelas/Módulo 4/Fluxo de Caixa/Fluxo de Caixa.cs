﻿using Company_seguros.DB.Fluxo_de_caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas.Fluxo_de_caixa
{
    public partial class Fluxo_de_Caixa : Form
    {
        public Fluxo_de_Caixa()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Fluxo_de_Caixa_Load(object sender, EventArgs e)
        {

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
        }
        void Consultar()
        {
            DateTime FirstDate = dtp1.Value.Date;
            DateTime SecondDate = dtp2.Value.Date;

            FluxoCaixaView view = new FluxoCaixaView();
            dgvFinanceiro.AutoGenerateColumns = false;
            dgvFinanceiro.DataSource = view.Consultar(FirstDate, SecondDate);
        }
    }
}
