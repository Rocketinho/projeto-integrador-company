﻿using Company_seguros.DB.Módulo_3.Venda.View;
using Company_seguros.DB.Módulo_4.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._5Estoque
{
    public partial class ConsultarEstoque : Form
    {
        public ConsultarEstoque()
        {
            InitializeComponent();
            ConfigurarGrid();
        }

        void ConfigurarGrid()
        {
            dgvProduto.AutoGenerateColumns = false;
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
      
        }

        void Buscar()
        {
            DTO_EstoqueView dto = new DTO_EstoqueView();
            dto.Produto = txtProduto.Text;

            View_Estoque view = new View_Estoque();
            List<DTO_EstoqueView> lista = view.Consultar(dto);
            dgvProduto.DataSource = lista;

        }

        private void ConsultarEstoque_Load(object sender, EventArgs e)
        {
        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        
    }
}
