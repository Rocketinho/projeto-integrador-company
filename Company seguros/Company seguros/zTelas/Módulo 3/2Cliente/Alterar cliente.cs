﻿using Company_seguros.DB.Cliente;
using Company_seguros.Ferramentas;
using Company_seguros.Ferramentas.Localizacao;
using Company_seguros.zTelas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._2Cliente
{
    public partial class Alterar_cliente : Form
    {
        public Alterar_cliente()
        {
            InitializeComponent();
            CarregarCombos();
        }


        DTO_Cliente dto;
        public void LoadScreen(DTO_Cliente dto)
        {
            this.dto = dto;
            txtNome.Text = dto.Cliente;
            txtRG.Text = dto.RG;
            txtCPF.Text = dto.CPF;
            txtCNPJ.Text = dto.CNPJ;
            dtpNascimento.Value = dto.Nascimento;
            txtEndereco.Text = dto.Endereco;
            txtNumero.Text = dto.Numero;
            txtCidade.Text = dto.Cidade;
            cboUF.SelectedItem = dto.UF;
            txtComplemento.Text = dto.Complemento;
            txtEmail.Text = dto.Email;
            txtTelefone.Text = dto.Telefone;
        }

        private void CarregarCombos()
        {

            Localizacao localizacao = new Localizacao();
            cboUF.DataSource = localizacao.UF();

        }


        private void Alterar_cliente_Load(object sender, EventArgs e)
        {

        }

        private void consultarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void editarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente tela = new Cadastrar_Cliente();
            tela.Show();
            Hide();
        }

        private void consultarClienteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            BuscarCliente tela = new BuscarCliente();
            tela.Show();
            Hide();
        }

        private void cadrastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void cboUF_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void alterarClienteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void cadrastrarClienteToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
        }

        private void consultarClienteToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            BuscarCliente frm = new BuscarCliente();
            frm.Show();
            this.Hide();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
           
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAlterar_Click_1(object sender, EventArgs e)
        {
            Alterar();
        }

        void Alterar()
        {
            Nulos nulos = new Nulos();
            Sexo sexo = new Sexo();

            this.dto.Cliente = txtNome.Text;
            this.dto.RG = nulos.VerificarRG(txtRG.Text);
            this.dto.CPF = nulos.VerificarCPF(txtCPF.Text);
            this.dto.CNPJ = nulos.VerificarCNPJ(txtCNPJ.Text);
            this.dto.Nascimento = dtpNascimento.Value;
            bool masculino = chkMasculino.Checked;
            bool feminino = chkFeminino.Checked;
            this.dto.Sexo = sexo.VerificarSexo(masculino, feminino);
            this.dto.Endereco = txtEndereco.Text;
            this.dto.Numero = txtNumero.Text;
            this.dto.Cidade = txtCidade.Text;
            this.dto.UF = cboUF.SelectedItem.ToString();
            this.dto.Complemento = txtComplemento.Text;
            this.dto.Email = txtEmail.Text;
            this.dto.Telefone = txtTelefone.Text;

            Business_Cliente business = new Business_Cliente();
            business.Alterar(dto);

            MessageBox.Show("Alterado com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            MENU menu = new MENU();
            menu.Show();
            this.Hide();
        }

        private void txtCNPJ_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void dtpNascimento_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtRG_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void chkMasculino_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkFeminino_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtEndereco_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void txtComplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Alterar();
            }
        }
    }
}
