﻿using Company_seguros.DB.Cliente;
using Company_seguros.Ferramentas;
using Company_seguros.zTelas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._2Cliente
{
    public partial class BuscarCliente : Form
    {
        public BuscarCliente()
        {
            InitializeComponent();
            CarregarPermissões();
        }
        private void CarregarPermissões()
        {
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }

            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
        }



        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cadrastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide(); ;
        }
       
        private void Consultar(DTO_Cliente dto)
        {
            Nulos nulos = new Nulos();
            dto.CPF = nulos.VerificarCPF(txtCPF.Text);
            dto.Cliente = txtnome.Text;
            Business_Cliente business = new Business_Cliente();
            List<DTO_Cliente> lista = business.Consultar(dto);
            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = lista;
             
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
        }

        private void BuscarCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit(); 
        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 5)
            {
                DTO_Cliente dto = dgvCliente.Rows[e.RowIndex].DataBoundItem as DTO_Cliente;
                Alterar_cliente frm = new Alterar_cliente();
                frm.LoadScreen(dto);
                frm.Show();
                this.Hide();

            }

                    

            if(e.ColumnIndex == 6)
            {
                DTO_Cliente dto = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Company", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(resp == DialogResult.Yes)
                {
                    Business_Cliente business = new Business_Cliente();
                    business.Remover(dto);
                    MessageBox.Show("Deletado com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DTO_Cliente dt = new DTO_Cliente();
                    Consultar(dt);
                }

            }
        }

        private void BuscarCliente_Load(object sender, EventArgs e)
        {

        }

    

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Consultar(dto);
        }

        private void txtnome_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                DTO_Cliente dto = new DTO_Cliente();
                Consultar(dto);
            }
        }
    }
}
