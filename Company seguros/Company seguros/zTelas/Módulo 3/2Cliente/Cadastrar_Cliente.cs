﻿using Company_seguros.DB._base;
using Company_seguros.DB.Cliente;
using Company_seguros.Ferramentas;
using Company_seguros.Ferramentas.Localizacao;
using Company_seguros.zTelas._2Cliente;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas.Cliente
{
    public partial class Cadastrar_Cliente : Form
    {
        public Cadastrar_Cliente()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void CarregarCombos()
        {
            
            Localizacao localizacao = new Localizacao();
            cboUF.ValueMember = nameof(Localizacao.UF);
            cboUF.DisplayMember = nameof(Localizacao.UF);
            List<string> lista = localizacao.UF();
            cboUF.DataSource = lista;
          
        }
        

        private void label10_Click(object sender, EventArgs e)
        {
            
        }

        private void Cadrastrar_cliente_Load(object sender, EventArgs e)
        {
            
        }
        private void Salvar(DTO_Cliente dto)
        {
            Nulos nulos = new Nulos();
            try
            {
                Sexo sexo = new Sexo();

                dto.Cliente = txtNome.Text;
                dto.RG = nulos.VerificarRG(txtRG.Text);
                dto.CPF = nulos.VerificarCPF(txtCPF.Text);
                dto.CNPJ = nulos.VerificarCNPJ(txtCNPJ.Text);
                dto.Nascimento = dtpNascimento.Value;
                bool masculino = chkMasculino.Checked;
                bool feminino = chkFeminino.Checked;
                dto.Sexo = sexo.VerificarSexo(masculino, feminino);
                dto.Endereco = txtEndereco.Text;
                dto.Numero = txtNumero.Text;
                dto.Cidade = txtCidade.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;

                Business_Cliente business = new Business_Cliente();
                business.Salvar(dto);
                
                MessageBox.Show("Salvo com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch 
            //{
            //    MessageBox.Show("Verifique se os dados estão corretos", "Company", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }



        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void Cadastrar_Cliente_Enter(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Salvar(dto);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DTO_Cliente dto = new DTO_Cliente();
                Salvar(dto);
            }
        }

        private void cboCidade_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Cadastrar_Cliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

      

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label27_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Salvar(dto);
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void txtNome_KeyDown_1(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                DTO_Cliente dto = new DTO_Cliente();
                Salvar(dto);
            }
        }
    }



   

}
