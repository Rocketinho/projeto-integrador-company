﻿using Company_seguros.DB.Cliente;
using Company_seguros.DB.Estoque;
using Company_seguros.DB.Funcionario;
using Company_seguros.DB.Módulo_3.Venda;
using Company_seguros.DB.MODULO2.Compra;
using Company_seguros.DB.MODULO2.Produto;
using Company_seguros.DB.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class Novavenda : Form
    {
        BindingList<DTO_Produto> carrinho = new BindingList<DTO_Produto>();

        public Novavenda()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarGrid();
        }

        DTO_Cliente cliente = new DTO_Cliente();

        void CarregarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = carrinho;


            Business_Cliente business = new Business_Cliente();
            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = business.Listar();

        }

        void CarregarCombos()
        {
            // Carregar Produto
            Business_Produto business = new Business_Produto();
            List<DTO_Produto> lista = business.Listar();
            cboProduto.ValueMember = nameof(DTO_Produto.ID);
            cboProduto.DisplayMember = nameof(DTO_Produto.Produto);
            cboProduto.DataSource = lista;

            //Carregar Funcionario
            Business_Funcionario busines = new Business_Funcionario();
            List<DTO_Funcionario> lista2 = busines.Listar();
            cboFuncionario.ValueMember = nameof(DTO_Funcionario.Id);
            cboFuncionario.DisplayMember = nameof(DTO_Funcionario.Funcionario);
            cboFuncionario.DataSource = lista2;

            // Carregar Serviços

            Business_ProdutoVenda b = new Business_ProdutoVenda();
            List<DTO_ProdutoVenda> lista3 = b.Listar();
            cboServiço.ValueMember = nameof(DTO_ProdutoVenda.ID);
            cboServiço.DisplayMember = nameof(DTO_ProdutoVenda.Servico);
            cboServiço.DataSource = lista3;
            
            

        }

        private void produtosAVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosVenda tela = new ProdutosVenda();
            tela.Show();
            Hide();
        }

        private void addNovoProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarProdutoVenda tela = new CadastrarProdutoVenda();
            tela.Show();
            Hide();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 2)
            {
                DTO_Produto dto = dgvItens.CurrentRow.DataBoundItem as DTO_Produto;
                DialogResult resp = MessageBox.Show("Deseja realmente excluir?", "Company", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resp == DialogResult.Yes)
                {
                    carrinho.Remove(dto);
                }
            }
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           

        }

        private void dgvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)       
        {

           

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void Novavenda_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCalcularTotal_Click(object sender, EventArgs e)
        {
           

        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = dgvCliente.CurrentRow.DataBoundItem as DTO_Cliente;
            this.cliente = dto;
            MessageBox.Show("Cliente Escolhido", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void btnCarrinho_Click(object sender, EventArgs e)
        {
            DTO_Produto dto = cboProduto.SelectedItem as DTO_Produto;

            for (int i = 0; i < nudQuantidade.Value; i++)
            {
                carrinho.Add(dto);
            }
            CarregarGrid();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            decimal precoProdutos = 0;
            foreach (DTO_Produto item in carrinho)
            {
                precoProdutos = item.Preco + precoProdutos;

            }

            DTO_ProdutoVenda servico = cboServiço.SelectedItem as DTO_ProdutoVenda;
            decimal total = precoProdutos + servico.Precoinstalacao;

            lblTotal.Text = total.ToString();
        }

        private void btnVenda_Click(object sender, EventArgs e)
        {
            DTO_Funcionario dtoFuncionario = new DTO_Funcionario();
            dtoFuncionario = cboFuncionario.SelectedItem as DTO_Funcionario;

            DTO_ProdutoVenda venda = cboServiço.SelectedItem as DTO_ProdutoVenda;

            Business_Venda business = new Business_Venda();
            business.Salvar(carrinho.ToList(), cliente, dtoFuncionario, venda);



            // Estoque

            foreach (DTO_Produto item in carrinho)
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.IdProduto = item.ID;

                Business_Estoque b = new Business_Estoque();
                dto = b.Consultar(dto);

                dto.Quantidade = dto.Quantidade - 1;

                b.Alterar(dto);

            }

            MessageBox.Show("Venda Finalizada", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);


            MENU menu = new MENU();
            menu.Show();
            this.Hide();
        }
    }
}
