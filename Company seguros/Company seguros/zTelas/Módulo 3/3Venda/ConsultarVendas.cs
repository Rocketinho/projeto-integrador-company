﻿using Company_seguros.DB.Módulo_3.Venda.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class ProdutosVenda : Form
    {
        public ProdutosVenda()
        {
            InitializeComponent();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
        private void Consultar(DTO_View dto)
        {
            DB.Módulo_3.Venda.View.View view = new DB.Módulo_3.Venda.View.View();
            dto.Cliente = txtCliente.Text;
                
            // Consultando e Passando valores pra grid
            dgvVenda.AutoGenerateColumns = false;
            dgvVenda.DataSource = view.Consultar(dto);

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
        }

        private void dgvVenda_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ProdutosVenda_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ProdutosVenda_Load(object sender, EventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DTO_View dto = new DTO_View();
            Consultar(dto);
        }

        private void txtCliente_KeyDown(object sender, KeyEventArgs e)
        {
           if(e.KeyCode == Keys.Enter)
            {
                DTO_View dto = new DTO_View();
                Consultar(dto);
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
