﻿using Company_seguros.DB.ProdutoVenda;
using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class Buscar_ProdutosVenda : Form
    {
        public Buscar_ProdutosVenda()
        {
            InitializeComponent();

            CarregarPermissões();
        }
        private void CarregarPermissões()
        {
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }

            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 3)
            {
                DTO_ProdutoVenda dto = dgvProduto.CurrentRow.DataBoundItem as DTO_ProdutoVenda;
                AlterarProdutoVenda frm = new AlterarProdutoVenda();
                frm.Loadscreen(dto);
                frm.Show();
                this.Hide();

            }

            if (e.ColumnIndex == 4)
            {
                DTO_ProdutoVenda dto = dgvProduto.CurrentRow.DataBoundItem as DTO_ProdutoVenda;
                DialogResult resultado = MessageBox.Show("Deseja realmente excluir?", "Company", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(resultado == DialogResult.Yes)
                {
                    Business_ProdutoVenda business = new Business_ProdutoVenda();
                    business.Remover(dto);
                    MessageBox.Show("Removido com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Buscar(dto);

                }
            }
        }

        private void Buscar(DTO_ProdutoVenda dto)
        {
            dto.Servico = txtProduto.Text;
            Business_ProdutoVenda business = new Business_ProdutoVenda();
            List<DTO_ProdutoVenda> lista = business.Consultar(dto);
            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = lista;

        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            
        }

        private void Buscar_ProdutosVenda_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void cadrastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DTO_ProdutoVenda dto = new DTO_ProdutoVenda();
            Buscar(dto);
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Buscar_ProdutosVenda_Load(object sender, EventArgs e)
        {

        }
    }
}
