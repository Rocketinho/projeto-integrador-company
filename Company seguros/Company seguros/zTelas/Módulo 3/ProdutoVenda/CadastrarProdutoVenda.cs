﻿using Company_seguros.DB.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class CadastrarProdutoVenda : Form
    {
        public CadastrarProdutoVenda()
        {
            InitializeComponent();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU mn = new MENU();
            mn.Show();
            Hide();
        }
        private void Salvar(DTO_ProdutoVenda dto)
        {
            dto.Servico = txtProduto.Text;
            dto.Precoinstalacao = Convert.ToDecimal(txtPreco.Text);

            Business_ProdutoVenda business = new Business_ProdutoVenda();
            business.Salvar(dto);

            MessageBox.Show("Salvo com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
          
        }

        private void CadastrarProdutoVenda_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            DTO_ProdutoVenda dto = new DTO_ProdutoVenda();
            Salvar(dto);

        }

        private void txtProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {

                DTO_ProdutoVenda dto = new DTO_ProdutoVenda();
                Salvar(dto);
            }
        }
    }
}
