﻿using Company_seguros.DB.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class AlterarProdutoVenda : Form
    {
        public AlterarProdutoVenda()
        {
            InitializeComponent();
        }

        DTO_ProdutoVenda dto;

        public void Loadscreen(DTO_ProdutoVenda dto)
        {
            this.dto = dto;
            txtProduto.Text = dto.Servico;
            txtPreco.Text = dto.Precoinstalacao.ToString();
        }

        private void AlterarProdutoVenda_Load(object sender, EventArgs e)
        {

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
           
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAlterar_Click_1(object sender, EventArgs e)
        {
            Alterar();
        }

        void Alterar()
        {

            this.dto.Servico = txtProduto.Text;
            this.dto.Precoinstalacao = Convert.ToDecimal(txtPreco.Text);


            Business_ProdutoVenda business = new Business_ProdutoVenda();
            business.Alterar(dto);

            MessageBox.Show("Alterado com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            MENU menu = new MENU();
            menu.Show();
            this.Hide();
        }

        private void txtPreco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                
                Alterar();
            }
        }
    }
}
