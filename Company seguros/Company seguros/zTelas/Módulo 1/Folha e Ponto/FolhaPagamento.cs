﻿using Company_seguros.DB.Ferramentas;
using Company_seguros.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._8RH
{
    public partial class FolhaPagamento : Form
    {
        public FolhaPagamento()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
           
        }

        private void btnGerarPagamento_Click(object sender, EventArgs e)
        {
         
        }

        private void btnBuscarFuncionario_Click(object sender, EventArgs e)
        {
            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Funcionario = txtFuncionario.Text;
            
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista = business.ConsultarFolha(dto);

            foreach (DTO_Funcionario item in lista)
            {
                dto = item;
            }

            lblSalario.Text = dto.Salario.ToString();
            lblTransporte.Text = dto.Transporte.ToString();
            lblRefeicao.Text = dto.Refeicao.ToString();
            lblAlimentacao.Text = dto.Alimentacao.ToString();
            lblConvenio.Text = dto.Convenio.ToString();
        }

        
        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        //private void Relatorio()
        //{
        //    //Variáveis "escopo" 
        //    string nome,
        //           //cargo
        //           diastrabalhados,
        //           valealimentacao = string.Empty,
        //           planosaude = string.Empty,
        //           horastotal,
        //           salariototal,
        //           conteudo;

        //    //Pegando os valores dos controles
        //    //cboCargo = cboCargo.SelectedItem;
        //    nome = txtNomeFuncionario.Text;
        //    diastrabalhados = nudDiasTrabalhados.Value.ToString();

        //    //Passando o valor em texto da RadioButton
        //    if (rdnSimS.Checked == true)
        //    {
        //        planosaude = "Sim";
        //    }
        //    else
        //    {
        //        planosaude = "Não";
        //    }

        //    //Novamente, o mesmo processo!
        //    if (rdnSimA.Checked == true)
        //    {
        //        valealimentacao = "Sim";
        //    }
        //    else
        //    {
        //        valealimentacao = "Não";
        //    }

        //    //Continue pegando o valor dos controles
        //    horastotal = txtHorasTrabalhadas.Text;

        //    //Chamo o Método SalarioHora
        //    salariototal = SalarioHora().ToString();

        //    conteudo = @"O funcionário " + nome + "\n" +
        //                "Trabalhou durante " + diastrabalhados + " dias, " +
        //                " por " + horastotal + " horas " + "\n" +
        //                "Recendo o equivalente à R$: " + salariototal + ",00 " + "\n\n" +
        //                "Desse valor, possui os descontos de: " + "\n" +
        //                "Plano de sáude: " + planosaude + "\n" +
        //                "Vale Alimentação: " + valealimentacao;

        //    //Passo o valor para o método PDF;
        //    PDF pdf = new PDF();
        //    pdf.Pdf(conteudo);

        //}
        ////declaro as variáveis de escopo
        //int diastrabalhados = 0;
        //decimal salariohora, horastrabalhadas, valealimentacao, planosaude, horastotal, salario, salariototal;

        private void nudPlanoSaude_ValueChanged(object sender, EventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //private decimal SalarioHora()
        //{
        //    try
        //    {


        //        //Pego a quantidade de dias trabalhados
        //        diastrabalhados = Convert.ToInt32(nudDiasTrabalhados.Value);

        //        //Pego quanto o funcionário ganha por hora
        //        salariohora = nudSalarioHora.Value;

        //        //Pego a quantidade de horas trabalhadas
        //        horastrabalhadas = Convert.ToDecimal(txtHorasTrabalhadas.Text);

        //        //Multiplicado os dias trabalhados com as horas trabalhadas para saber o valor total.
        //        horastotal = salariohora * horastrabalhadas;

        //        //De acordo com o total de horas multiplicado com suas horas de trabalho, eu declararei o seu sálario
        //        salario = diastrabalhados * horastotal;

        //        //Caso tenho plano de saúde
        //        if (rdnSimS.Checked == true)
        //        {
        //            planosaude = nudPlanoSaude.Value;
        //        }
        //        //Caso tenho Vale alimentação
        //        if (rdnSimA.Checked == true)
        //        {
        //            valealimentacao = nudVA.Value;
        //        }
        //        salariototal = salario - valealimentacao - planosaude;

        //    }
        //    catch
        //    {
        //        MessageBox.Show("Verifique se nenhuma informação está faltando", "Erro no sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return salariototal;
        //}
    }
}
