﻿namespace Company_seguros.zTelas._1Usuario
{
    partial class UsuarioAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNome = new System.Windows.Forms.TextBox();
            this.chkConsultarCompras = new System.Windows.Forms.CheckBox();
            this.chkSalvarCompras = new System.Windows.Forms.CheckBox();
            this.chkAlterarCompras = new System.Windows.Forms.CheckBox();
            this.chkConsultarEstoque = new System.Windows.Forms.CheckBox();
            this.chkSalvarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarEstoque = new System.Windows.Forms.CheckBox();
            this.chkRemoverEstoque = new System.Windows.Forms.CheckBox();
            this.chkRemoverCompras = new System.Windows.Forms.CheckBox();
            this.gpbEstoque = new System.Windows.Forms.GroupBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.gpbFinanceiro = new System.Windows.Forms.GroupBox();
            this.chkConsultarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkSalvarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkAlterarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRemoverFinanceiro = new System.Windows.Forms.CheckBox();
            this.gpbCompras = new System.Windows.Forms.GroupBox();
            this.gpbRH = new System.Windows.Forms.GroupBox();
            this.chkConsultarRH = new System.Windows.Forms.CheckBox();
            this.chkSalvarRH = new System.Windows.Forms.CheckBox();
            this.chkAlterarRH = new System.Windows.Forms.CheckBox();
            this.chkRemoverRH = new System.Windows.Forms.CheckBox();
            this.gpbFuncionario = new System.Windows.Forms.GroupBox();
            this.chkConsultarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkSalvarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkRemoverFuncionario = new System.Windows.Forms.CheckBox();
            this.gpbVendas = new System.Windows.Forms.GroupBox();
            this.chkConsultarVendas = new System.Windows.Forms.CheckBox();
            this.chkSalvarVendas = new System.Windows.Forms.CheckBox();
            this.chkAlterarVendas = new System.Windows.Forms.CheckBox();
            this.chkRemoverVendas = new System.Windows.Forms.CheckBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.nudVT = new System.Windows.Forms.NumericUpDown();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.voltarAoMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label17 = new System.Windows.Forms.Label();
            this.gpbEstoque.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gpbFinanceiro.SuspendLayout();
            this.gpbCompras.SuspendLayout();
            this.gpbRH.SuspendLayout();
            this.gpbFuncionario.SuspendLayout();
            this.gpbVendas.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtNome.Location = new System.Drawing.Point(127, 19);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(220, 26);
            this.txtNome.TabIndex = 0;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // chkConsultarCompras
            // 
            this.chkConsultarCompras.AutoSize = true;
            this.chkConsultarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarCompras.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarCompras.Name = "chkConsultarCompras";
            this.chkConsultarCompras.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarCompras.TabIndex = 41;
            this.chkConsultarCompras.Text = "Consultar";
            this.chkConsultarCompras.UseVisualStyleBackColor = true;
            // 
            // chkSalvarCompras
            // 
            this.chkSalvarCompras.AutoSize = true;
            this.chkSalvarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarCompras.Location = new System.Drawing.Point(109, 25);
            this.chkSalvarCompras.Name = "chkSalvarCompras";
            this.chkSalvarCompras.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarCompras.TabIndex = 42;
            this.chkSalvarCompras.Text = "Salvar";
            this.chkSalvarCompras.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCompras
            // 
            this.chkAlterarCompras.AutoSize = true;
            this.chkAlterarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarCompras.Location = new System.Drawing.Point(6, 67);
            this.chkAlterarCompras.Name = "chkAlterarCompras";
            this.chkAlterarCompras.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarCompras.TabIndex = 43;
            this.chkAlterarCompras.Text = "Alterar";
            this.chkAlterarCompras.UseVisualStyleBackColor = true;
            // 
            // chkConsultarEstoque
            // 
            this.chkConsultarEstoque.AutoSize = true;
            this.chkConsultarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarEstoque.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarEstoque.Name = "chkConsultarEstoque";
            this.chkConsultarEstoque.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarEstoque.TabIndex = 41;
            this.chkConsultarEstoque.Text = "Consultar";
            this.chkConsultarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkSalvarEstoque
            // 
            this.chkSalvarEstoque.AutoSize = true;
            this.chkSalvarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarEstoque.Location = new System.Drawing.Point(100, 25);
            this.chkSalvarEstoque.Name = "chkSalvarEstoque";
            this.chkSalvarEstoque.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarEstoque.TabIndex = 42;
            this.chkSalvarEstoque.Text = "Salvar";
            this.chkSalvarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarEstoque
            // 
            this.chkAlterarEstoque.AutoSize = true;
            this.chkAlterarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarEstoque.Location = new System.Drawing.Point(6, 67);
            this.chkAlterarEstoque.Name = "chkAlterarEstoque";
            this.chkAlterarEstoque.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarEstoque.TabIndex = 43;
            this.chkAlterarEstoque.Text = "Alterar";
            this.chkAlterarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkRemoverEstoque
            // 
            this.chkRemoverEstoque.AutoSize = true;
            this.chkRemoverEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverEstoque.Location = new System.Drawing.Point(100, 67);
            this.chkRemoverEstoque.Name = "chkRemoverEstoque";
            this.chkRemoverEstoque.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverEstoque.TabIndex = 44;
            this.chkRemoverEstoque.Text = "Remover";
            this.chkRemoverEstoque.UseVisualStyleBackColor = true;
            // 
            // chkRemoverCompras
            // 
            this.chkRemoverCompras.AutoSize = true;
            this.chkRemoverCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverCompras.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverCompras.Location = new System.Drawing.Point(109, 67);
            this.chkRemoverCompras.Name = "chkRemoverCompras";
            this.chkRemoverCompras.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverCompras.TabIndex = 44;
            this.chkRemoverCompras.Text = "Remover";
            this.chkRemoverCompras.UseVisualStyleBackColor = true;
            // 
            // gpbEstoque
            // 
            this.gpbEstoque.Controls.Add(this.chkConsultarEstoque);
            this.gpbEstoque.Controls.Add(this.chkSalvarEstoque);
            this.gpbEstoque.Controls.Add(this.chkAlterarEstoque);
            this.gpbEstoque.Controls.Add(this.chkRemoverEstoque);
            this.gpbEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbEstoque.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbEstoque.Location = new System.Drawing.Point(427, 151);
            this.gpbEstoque.Name = "gpbEstoque";
            this.gpbEstoque.Size = new System.Drawing.Size(187, 119);
            this.gpbEstoque.TabIndex = 11;
            this.gpbEstoque.TabStop = false;
            this.gpbEstoque.Text = "Estoque";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(643, 422);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissão";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnAlterar);
            this.groupBox5.Controls.Add(this.gpbEstoque);
            this.groupBox5.Controls.Add(this.gpbFinanceiro);
            this.groupBox5.Controls.Add(this.gpbCompras);
            this.groupBox5.Controls.Add(this.gpbRH);
            this.groupBox5.Controls.Add(this.gpbFuncionario);
            this.groupBox5.Controls.Add(this.gpbVendas);
            this.groupBox5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(631, 410);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Permissão";
            // 
            // btnAlterar
            // 
            this.btnAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.btnAlterar.ForeColor = System.Drawing.Color.Black;
            this.btnAlterar.Location = new System.Drawing.Point(256, 297);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(93, 60);
            this.btnAlterar.TabIndex = 30;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // gpbFinanceiro
            // 
            this.gpbFinanceiro.Controls.Add(this.chkConsultarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkSalvarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkAlterarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkRemoverFinanceiro);
            this.gpbFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbFinanceiro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFinanceiro.Location = new System.Drawing.Point(19, 151);
            this.gpbFinanceiro.Name = "gpbFinanceiro";
            this.gpbFinanceiro.Size = new System.Drawing.Size(196, 119);
            this.gpbFinanceiro.TabIndex = 9;
            this.gpbFinanceiro.TabStop = false;
            this.gpbFinanceiro.Text = "Financeiro";
            // 
            // chkConsultarFinanceiro
            // 
            this.chkConsultarFinanceiro.AutoSize = true;
            this.chkConsultarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFinanceiro.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFinanceiro.Name = "chkConsultarFinanceiro";
            this.chkConsultarFinanceiro.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarFinanceiro.TabIndex = 41;
            this.chkConsultarFinanceiro.Text = "Consultar";
            this.chkConsultarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFinanceiro
            // 
            this.chkSalvarFinanceiro.AutoSize = true;
            this.chkSalvarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFinanceiro.Location = new System.Drawing.Point(98, 25);
            this.chkSalvarFinanceiro.Name = "chkSalvarFinanceiro";
            this.chkSalvarFinanceiro.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarFinanceiro.TabIndex = 42;
            this.chkSalvarFinanceiro.Text = "Salvar";
            this.chkSalvarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFinanceiro
            // 
            this.chkAlterarFinanceiro.AutoSize = true;
            this.chkAlterarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFinanceiro.Location = new System.Drawing.Point(6, 67);
            this.chkAlterarFinanceiro.Name = "chkAlterarFinanceiro";
            this.chkAlterarFinanceiro.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarFinanceiro.TabIndex = 43;
            this.chkAlterarFinanceiro.Text = "Alterar";
            this.chkAlterarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFinanceiro
            // 
            this.chkRemoverFinanceiro.AutoSize = true;
            this.chkRemoverFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFinanceiro.Location = new System.Drawing.Point(92, 67);
            this.chkRemoverFinanceiro.Name = "chkRemoverFinanceiro";
            this.chkRemoverFinanceiro.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverFinanceiro.TabIndex = 44;
            this.chkRemoverFinanceiro.Text = "Remover";
            this.chkRemoverFinanceiro.UseVisualStyleBackColor = true;
            // 
            // gpbCompras
            // 
            this.gpbCompras.Controls.Add(this.chkConsultarCompras);
            this.gpbCompras.Controls.Add(this.chkSalvarCompras);
            this.gpbCompras.Controls.Add(this.chkAlterarCompras);
            this.gpbCompras.Controls.Add(this.chkRemoverCompras);
            this.gpbCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbCompras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbCompras.Location = new System.Drawing.Point(221, 151);
            this.gpbCompras.Name = "gpbCompras";
            this.gpbCompras.Size = new System.Drawing.Size(200, 119);
            this.gpbCompras.TabIndex = 10;
            this.gpbCompras.TabStop = false;
            this.gpbCompras.Text = "Compras";
            // 
            // gpbRH
            // 
            this.gpbRH.Controls.Add(this.chkConsultarRH);
            this.gpbRH.Controls.Add(this.chkSalvarRH);
            this.gpbRH.Controls.Add(this.chkAlterarRH);
            this.gpbRH.Controls.Add(this.chkRemoverRH);
            this.gpbRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbRH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbRH.Location = new System.Drawing.Point(427, 25);
            this.gpbRH.Name = "gpbRH";
            this.gpbRH.Size = new System.Drawing.Size(187, 109);
            this.gpbRH.TabIndex = 8;
            this.gpbRH.TabStop = false;
            this.gpbRH.Text = "RH";
            // 
            // chkConsultarRH
            // 
            this.chkConsultarRH.AutoSize = true;
            this.chkConsultarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarRH.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarRH.Location = new System.Drawing.Point(5, 22);
            this.chkConsultarRH.Name = "chkConsultarRH";
            this.chkConsultarRH.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarRH.TabIndex = 41;
            this.chkConsultarRH.Text = "Consultar";
            this.chkConsultarRH.UseVisualStyleBackColor = true;
            // 
            // chkSalvarRH
            // 
            this.chkSalvarRH.AutoSize = true;
            this.chkSalvarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarRH.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarRH.Location = new System.Drawing.Point(104, 22);
            this.chkSalvarRH.Name = "chkSalvarRH";
            this.chkSalvarRH.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarRH.TabIndex = 42;
            this.chkSalvarRH.Text = "Salvar";
            this.chkSalvarRH.UseVisualStyleBackColor = true;
            // 
            // chkAlterarRH
            // 
            this.chkAlterarRH.AutoSize = true;
            this.chkAlterarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarRH.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarRH.Location = new System.Drawing.Point(6, 66);
            this.chkAlterarRH.Name = "chkAlterarRH";
            this.chkAlterarRH.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarRH.TabIndex = 43;
            this.chkAlterarRH.Text = "Alterar";
            this.chkAlterarRH.UseVisualStyleBackColor = true;
            // 
            // chkRemoverRH
            // 
            this.chkRemoverRH.AutoSize = true;
            this.chkRemoverRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverRH.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverRH.Location = new System.Drawing.Point(104, 66);
            this.chkRemoverRH.Name = "chkRemoverRH";
            this.chkRemoverRH.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverRH.TabIndex = 44;
            this.chkRemoverRH.Text = "Remover";
            this.chkRemoverRH.UseVisualStyleBackColor = true;
            // 
            // gpbFuncionario
            // 
            this.gpbFuncionario.Controls.Add(this.chkConsultarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkSalvarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkAlterarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkRemoverFuncionario);
            this.gpbFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbFuncionario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFuncionario.Location = new System.Drawing.Point(221, 22);
            this.gpbFuncionario.Name = "gpbFuncionario";
            this.gpbFuncionario.Size = new System.Drawing.Size(200, 112);
            this.gpbFuncionario.TabIndex = 4;
            this.gpbFuncionario.TabStop = false;
            this.gpbFuncionario.Text = "Funcionario";
            // 
            // chkConsultarFuncionario
            // 
            this.chkConsultarFuncionario.AutoSize = true;
            this.chkConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFuncionario.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFuncionario.Name = "chkConsultarFuncionario";
            this.chkConsultarFuncionario.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarFuncionario.TabIndex = 41;
            this.chkConsultarFuncionario.Text = "Consultar";
            this.chkConsultarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFuncionario
            // 
            this.chkSalvarFuncionario.AutoSize = true;
            this.chkSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFuncionario.Location = new System.Drawing.Point(100, 25);
            this.chkSalvarFuncionario.Name = "chkSalvarFuncionario";
            this.chkSalvarFuncionario.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarFuncionario.TabIndex = 42;
            this.chkSalvarFuncionario.Text = "Salvar";
            this.chkSalvarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(6, 66);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarFuncionario.TabIndex = 43;
            this.chkAlterarFuncionario.Text = "Alterar";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFuncionario
            // 
            this.chkRemoverFuncionario.AutoSize = true;
            this.chkRemoverFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFuncionario.Location = new System.Drawing.Point(100, 66);
            this.chkRemoverFuncionario.Name = "chkRemoverFuncionario";
            this.chkRemoverFuncionario.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverFuncionario.TabIndex = 44;
            this.chkRemoverFuncionario.Text = "Remover";
            this.chkRemoverFuncionario.UseVisualStyleBackColor = true;
            // 
            // gpbVendas
            // 
            this.gpbVendas.Controls.Add(this.chkConsultarVendas);
            this.gpbVendas.Controls.Add(this.chkSalvarVendas);
            this.gpbVendas.Controls.Add(this.chkAlterarVendas);
            this.gpbVendas.Controls.Add(this.chkRemoverVendas);
            this.gpbVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbVendas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbVendas.Location = new System.Drawing.Point(19, 22);
            this.gpbVendas.Name = "gpbVendas";
            this.gpbVendas.Size = new System.Drawing.Size(196, 112);
            this.gpbVendas.TabIndex = 0;
            this.gpbVendas.TabStop = false;
            this.gpbVendas.Text = "Vendas";
            // 
            // chkConsultarVendas
            // 
            this.chkConsultarVendas.AutoSize = true;
            this.chkConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkConsultarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarVendas.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarVendas.Name = "chkConsultarVendas";
            this.chkConsultarVendas.Size = new System.Drawing.Size(88, 24);
            this.chkConsultarVendas.TabIndex = 41;
            this.chkConsultarVendas.Text = "Consultar";
            this.chkConsultarVendas.UseVisualStyleBackColor = true;
            // 
            // chkSalvarVendas
            // 
            this.chkSalvarVendas.AutoSize = true;
            this.chkSalvarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkSalvarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarVendas.Location = new System.Drawing.Point(98, 25);
            this.chkSalvarVendas.Name = "chkSalvarVendas";
            this.chkSalvarVendas.Size = new System.Drawing.Size(67, 24);
            this.chkSalvarVendas.TabIndex = 42;
            this.chkSalvarVendas.Text = "Salvar";
            this.chkSalvarVendas.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVendas
            // 
            this.chkAlterarVendas.AutoSize = true;
            this.chkAlterarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkAlterarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarVendas.Location = new System.Drawing.Point(6, 69);
            this.chkAlterarVendas.Name = "chkAlterarVendas";
            this.chkAlterarVendas.Size = new System.Drawing.Size(69, 24);
            this.chkAlterarVendas.TabIndex = 43;
            this.chkAlterarVendas.Text = "Alterar";
            this.chkAlterarVendas.UseVisualStyleBackColor = true;
            // 
            // chkRemoverVendas
            // 
            this.chkRemoverVendas.AutoSize = true;
            this.chkRemoverVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.chkRemoverVendas.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverVendas.Location = new System.Drawing.Point(98, 69);
            this.chkRemoverVendas.Name = "chkRemoverVendas";
            this.chkRemoverVendas.Size = new System.Drawing.Size(83, 24);
            this.chkRemoverVendas.TabIndex = 44;
            this.chkRemoverVendas.Text = "Remover";
            this.chkRemoverVendas.UseVisualStyleBackColor = true;
            // 
            // txtCPF
            // 
            this.txtCPF.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtCPF.Location = new System.Drawing.Point(227, 50);
            this.txtCPF.Mask = "000000000/00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(120, 26);
            this.txtCPF.TabIndex = 3;
            this.txtCPF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // txtRG
            // 
            this.txtRG.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtRG.Location = new System.Drawing.Point(42, 53);
            this.txtRG.Mask = "00,000,000-&";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(140, 26);
            this.txtRG.TabIndex = 2;
            this.txtRG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(188, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "CPF:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 20);
            this.label11.TabIndex = 5;
            this.label11.Text = "RG: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Completo:";
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkF.Location = new System.Drawing.Point(48, 15);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(31, 17);
            this.chkF.TabIndex = 1;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkM.Location = new System.Drawing.Point(6, 15);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(34, 17);
            this.chkM.TabIndex = 0;
            this.chkM.TabStop = true;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(83, 17);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(140, 26);
            this.txtUsuario.TabIndex = 0;
            this.txtUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtUsuario);
            this.groupBox6.Controls.Add(this.txtSenha);
            this.groupBox6.Controls.Add(this.lblUsuario);
            this.groupBox6.Controls.Add(this.lblSenha);
            this.groupBox6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox6.Location = new System.Drawing.Point(362, 217);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(259, 110);
            this.groupBox6.TabIndex = 51;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " ";
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(83, 54);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(140, 26);
            this.txtSenha.TabIndex = 1;
            this.txtSenha.UseSystemPasswordChar = true;
            this.txtSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.lblUsuario.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario.Location = new System.Drawing.Point(9, 17);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(61, 20);
            this.lblUsuario.TabIndex = 5;
            this.lblUsuario.Text = "Usuário:";
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.lblSenha.ForeColor = System.Drawing.Color.Black;
            this.lblSenha.Location = new System.Drawing.Point(11, 60);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(52, 20);
            this.lblSenha.TabIndex = 6;
            this.lblSenha.Text = "Senha:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(6, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 20);
            this.label15.TabIndex = 44;
            this.label15.Text = "Salário:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(11, 124);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 20);
            this.label16.TabIndex = 45;
            this.label16.Text = "VR:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.nudConvenio);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.nudVA);
            this.groupBox7.Controls.Add(this.nudSalario);
            this.groupBox7.Controls.Add(this.nudVT);
            this.groupBox7.Controls.Add(this.nudVR);
            this.groupBox7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox7.Location = new System.Drawing.Point(362, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(259, 192);
            this.groupBox7.TabIndex = 52;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(11, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 20);
            this.label12.TabIndex = 41;
            this.label12.Text = "VA:";
            // 
            // nudConvenio
            // 
            this.nudConvenio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.nudConvenio.ForeColor = System.Drawing.Color.Black;
            this.nudConvenio.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudConvenio.Location = new System.Drawing.Point(83, 153);
            this.nudConvenio.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(140, 26);
            this.nudConvenio.TabIndex = 4;
            this.nudConvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudConvenio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(9, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 20);
            this.label13.TabIndex = 42;
            this.label13.Text = "VT:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(6, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 20);
            this.label14.TabIndex = 43;
            this.label14.Text = "Convênio:";
            // 
            // nudVA
            // 
            this.nudVA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.nudVA.ForeColor = System.Drawing.Color.Black;
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(83, 83);
            this.nudVA.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(140, 26);
            this.nudVA.TabIndex = 2;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // nudSalario
            // 
            this.nudSalario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.nudSalario.ForeColor = System.Drawing.Color.Black;
            this.nudSalario.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalario.Location = new System.Drawing.Point(83, 15);
            this.nudSalario.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(140, 26);
            this.nudSalario.TabIndex = 0;
            this.nudSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudSalario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // nudVT
            // 
            this.nudVT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVT.DecimalPlaces = 2;
            this.nudVT.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.nudVT.ForeColor = System.Drawing.Color.Black;
            this.nudVT.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVT.Location = new System.Drawing.Point(83, 51);
            this.nudVT.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVT.Name = "nudVT";
            this.nudVT.Size = new System.Drawing.Size(140, 26);
            this.nudVT.TabIndex = 1;
            this.nudVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // nudVR
            // 
            this.nudVR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.nudVR.ForeColor = System.Drawing.Color.Black;
            this.nudVR.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVR.Location = new System.Drawing.Point(83, 118);
            this.nudVR.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(140, 26);
            this.nudVR.TabIndex = 3;
            this.nudVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudVR.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 422);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Funcionário";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.dtpNascimento);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Controls.Add(this.txtRG);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(10, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(627, 414);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados do Funcionário";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTelefone);
            this.groupBox4.Controls.Add(this.txtEmail);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(13, 271);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(334, 106);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contato";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(87, 56);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(232, 26);
            this.txtTelefone.TabIndex = 1;
            this.txtTelefone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(60, 23);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(259, 26);
            this.txtEmail.TabIndex = 0;
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 20);
            this.label9.TabIndex = 1;
            this.label9.Text = "Telefone 1:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Email:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCidade);
            this.groupBox3.Controls.Add(this.txtBairro);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtRua);
            this.groupBox3.Controls.Add(this.txtNumero);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(13, 121);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(334, 130);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Endereço";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(60, 93);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(204, 26);
            this.txtCidade.TabIndex = 3;
            this.txtCidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(60, 54);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(204, 26);
            this.txtBairro.TabIndex = 2;
            this.txtBairro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "Cidade:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "Bairro:";
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(49, 22);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(182, 26);
            this.txtRua.TabIndex = 0;
            this.txtRua.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(270, 22);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(49, 26);
            this.txtNumero.TabIndex = 1;
            this.txtNumero.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nº:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Rua:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNascimento.Location = new System.Drawing.Point(143, 87);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(101, 26);
            this.dtpNascimento.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(6, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Data de nascimento:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkF);
            this.groupBox2.Controls.Add(this.chkM);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(264, 76);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(83, 39);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sexo";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 477);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(647, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.tabControl1.Location = new System.Drawing.Point(-2, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 455);
            this.tabControl1.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(71)))), ((int)(((byte)(160)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voltarAoMenuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(647, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // voltarAoMenuToolStripMenuItem
            // 
            this.voltarAoMenuToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.voltarAoMenuToolStripMenuItem.Name = "voltarAoMenuToolStripMenuItem";
            this.voltarAoMenuToolStripMenuItem.Size = new System.Drawing.Size(111, 24);
            this.voltarAoMenuToolStripMenuItem.Text = "Voltar ao menu";
            this.voltarAoMenuToolStripMenuItem.Click += new System.EventHandler(this.voltarAoMenuToolStripMenuItem_Click_2);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(626, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 20);
            this.label17.TabIndex = 18;
            this.label17.Text = "X";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // UsuarioAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 499);
            this.ControlBox = false;
            this.Controls.Add(this.label17);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UsuarioAlterar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.UsuarioAlterar_Load);
            this.gpbEstoque.ResumeLayout(false);
            this.gpbEstoque.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.gpbFinanceiro.ResumeLayout(false);
            this.gpbFinanceiro.PerformLayout();
            this.gpbCompras.ResumeLayout(false);
            this.gpbCompras.PerformLayout();
            this.gpbRH.ResumeLayout(false);
            this.gpbRH.PerformLayout();
            this.gpbFuncionario.ResumeLayout(false);
            this.gpbFuncionario.PerformLayout();
            this.gpbVendas.ResumeLayout(false);
            this.gpbVendas.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.CheckBox chkConsultarCompras;
        private System.Windows.Forms.CheckBox chkSalvarCompras;
        private System.Windows.Forms.CheckBox chkAlterarCompras;
        private System.Windows.Forms.CheckBox chkConsultarEstoque;
        private System.Windows.Forms.CheckBox chkSalvarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarEstoque;
        private System.Windows.Forms.CheckBox chkRemoverEstoque;
        private System.Windows.Forms.CheckBox chkRemoverCompras;
        private System.Windows.Forms.GroupBox gpbEstoque;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox gpbFinanceiro;
        private System.Windows.Forms.CheckBox chkConsultarFinanceiro;
        private System.Windows.Forms.CheckBox chkSalvarFinanceiro;
        private System.Windows.Forms.CheckBox chkAlterarFinanceiro;
        private System.Windows.Forms.CheckBox chkRemoverFinanceiro;
        private System.Windows.Forms.GroupBox gpbCompras;
        private System.Windows.Forms.GroupBox gpbRH;
        private System.Windows.Forms.CheckBox chkConsultarRH;
        private System.Windows.Forms.CheckBox chkSalvarRH;
        private System.Windows.Forms.CheckBox chkAlterarRH;
        private System.Windows.Forms.CheckBox chkRemoverRH;
        private System.Windows.Forms.GroupBox gpbFuncionario;
        private System.Windows.Forms.CheckBox chkConsultarFuncionario;
        private System.Windows.Forms.CheckBox chkSalvarFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkRemoverFuncionario;
        private System.Windows.Forms.GroupBox gpbVendas;
        private System.Windows.Forms.CheckBox chkConsultarVendas;
        private System.Windows.Forms.CheckBox chkSalvarVendas;
        private System.Windows.Forms.CheckBox chkAlterarVendas;
        private System.Windows.Forms.CheckBox chkRemoverVendas;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.NumericUpDown nudVT;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem voltarAoMenuToolStripMenuItem;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnAlterar;
    }
}