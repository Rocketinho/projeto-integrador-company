﻿using Company_seguros.DB.Funcionario;
using Company_seguros.DB.Funcionario.Permissão;
using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._1Usuario
{
    public partial class UsuarioBuscar : Form
    {
        public UsuarioBuscar()
        {
            InitializeComponent();
            CarregarPermissoes();
        }
        private void CarregarPermissoes()
        {
            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        
        {
            CarregarGrid();
        }
        public void CarregarGrid()
        {
            DTO_Funcionario dto = new DTO_Funcionario();

            dto.Funcionario = txtNome.Text.Trim();
            if (txtCPF.Text == "         /")
            {
                dto.CPF = "";
            }
            else
            {
                dto.CPF = txtCPF.Text;
            }
            if (txtRG.Text == "  .   .   -")
            {
                dto.RG = "";
            }
            else
            {
                dto.RG = txtRG.Text;
            }
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista = business.Consultar(dto);

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void UsuarioBuscar_Load(object sender, EventArgs e)
        {

        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DTO_Funcionario dto_funcionario = dgvFuncionario.Rows[e.RowIndex].DataBoundItem as DTO_Funcionario;

                DialogResult resultado = MessageBox.Show("Deseja realmente excluir?", "Company-Seguros", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resultado == DialogResult.Yes)
                {
                    // Remover Acesso(antes do funcionario por causa da pk)

                    DTO_Acesso dto_acesso = new DTO_Acesso();
                    dto_acesso.ID_Funcionario = dto_funcionario.Id;

                    Business_Acesso b = new Business_Acesso();
                    b.Remover(dto_acesso);

                    //Remover Funcionário

                    Business_Funcionario business = new Business_Funcionario();
                    business.Remover(dto_funcionario);

                    CarregarGrid();
                }
            }
            if (e.ColumnIndex == 6)
            {
                DTO_Funcionario dto_funcionario = dgvFuncionario.Rows[e.RowIndex].DataBoundItem as DTO_Funcionario;
                UsuarioAlterar frm = new UsuarioAlterar();
                frm.Loadscreen(dto_funcionario, dto_funcionario.Id);
                frm.Show();
                    
                

            }
        }

      

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UsuarioBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }
    }
}
