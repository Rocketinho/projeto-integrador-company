﻿using Company_seguros.DB.Estoque;
using Company_seguros.DB.MODULO2.Produto;
using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class ProdutosCompra : Form
    {
        Business_Produto db = new Business_Produto();
        public ProdutosCompra()
        {
            InitializeComponent();
            CarregarPermissões();

        }

        private void CarregarPermissões()
        {
            if(UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }

            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
        }


        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
        //Passo o valor carregado no controle para o método de consultar, e depois, esse valor vai para a GRIED
        public void CarregarGrid()
        {
            DTO_Produto dto = new DTO_Produto();
            dto.Produto = txtProduto.Text;

            List<DTO_Produto> consult = db.Consultar(dto);

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = consult;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void dgvProduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Posição 3 da gried
            if (e.ColumnIndex == 2)
            {
                //Pego o valor da linha selecionada
                DTO_Produto linha = dgvProduto.CurrentRow.DataBoundItem as DTO_Produto;

                //Instacio o formulário da tela de produto
                AlterarProdutoCompra tela = new AlterarProdutoCompra();

                //Chamo o Método LoadScreen passando como parâmetro o valor encontrado na linha da gried
                tela.LoadScreen(linha);

                //Chamo o método OpenScreen presente na formulário onde contém o Menu
                tela.Show();

                //Oculto essa tela
                this.Hide();
            }
            //Posição 4 da gried
            if (e.ColumnIndex == 3)
            {
                //Pego o valor da linha selecionada
                DTO_Produto linha = dgvProduto.CurrentRow.DataBoundItem as DTO_Produto;

                //Mensagem para o Usuário
                DialogResult dialog = MessageBox.Show("Tem certeza que deseja apagar esse registro?",
                                                      "AVISO!!",
                                                      MessageBoxButtons.YesNo,
                                                      MessageBoxIcon.Question);

                if (dialog == DialogResult.Yes)
                {
                    // Apago do Estoque
                    Business_Estoque estoque = new Business_Estoque();
                    DTO_Estoque dt = new DTO_Estoque();

                    dt.IdProduto = linha.ID;

                    estoque.Remover(dt);

                    // Para Depois Apagar na tb_produto

                    //Instancio a Business
                    Business_Produto db = new Business_Produto();

                    //Chamo o método Remover e passo o valor do ID
                    db.Remover(linha.ID);

                    

                    //Carrego a gried.
                    CarregarGrid();
                }

            }
        }

        private void ProdutosCompra_Load(object sender, EventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CarregarGrid();

        }

        private void txtProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CarregarGrid();
            }
        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
        }
    }
}
