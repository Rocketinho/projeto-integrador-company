﻿using Company_seguros.DB.Estoque;
using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.DB.MODULO2.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class CadastrarProdutoCompra : Form
    {
        Business_Produto db = new Business_Produto();

        public CadastrarProdutoCompra()
        {
            InitializeComponent();
                //CarregarCombos();
        }
        //private void CarregarCombos()
        //{
        //    Business_Fornecedor db = new Business_Fornecedor();
        //    List<DTO_Fornecedor> list = db.Listar();

        //    cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
        //    cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);

        //    cboFornecedor.DataSource = list;
        //}
        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void produtosAVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCompra tela = new ProdutosCompra();

        }
        public int Salvar()
        {

            DTO_Produto dto = new DTO_Produto();

            //Passo o valor dos controles para o DTO.

            dto.Produto = txtProduto.Text;
            dto.Preco = nudPreco.Value;
            dto.Marca = txtMarca.Text;
           

            //Passo o valor do DTO para business.

            int id = db.Salvar(dto); 
            
            MessageBox.Show( "Produto Salvo com Sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return id;
        }

        public void SalvarEstoque(int id)
        {
            DTO_Estoque dto = new DTO_Estoque();
            dto.IdProduto = id;
            dto.Quantidade = 0;
            dto.QuantidadeMinima = Convert.ToInt32(nudMinima.Value);
            dto.QuantidadeMaxima = Convert.ToInt32(nudMaxima.Value);

            Business_Estoque business = new Business_Estoque();
            business.Salvar(dto);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
           
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            // Salva Produto
            int idProduto = Salvar();

            // Salva Produto no Estoque
            SalvarEstoque(idProduto);
        }

        private void txtProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // Salva Produto
                int idProduto = Salvar();

                // Salva Produto no Estoque
                SalvarEstoque(idProduto);
            }
        }
    }
}
