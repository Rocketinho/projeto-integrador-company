﻿using Company_seguros.DB.Módulo_2.Compra.View;
using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas.Módulo_2.Compra
{
    public partial class ConsultarCompra : Form
    {
        public ConsultarCompra()
        {
            InitializeComponent();
        }
       

            private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            
        }

        private void ConsultarCompra_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        private void Consultar()
        {
            DTO_ViewCompra dto = new DTO_ViewCompra();
            dto.Razao = txtFornecedor.Text;

            ViewCompra view = new ViewCompra();
            List<DTO_ViewCompra> lista = view.Consultar(dto);

            dgvCompra.AutoGenerateColumns = false;
            dgvCompra.DataSource = lista;
        }

        private void dgvCompra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtFornecedor_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Consultar();
            }
        }
    }
}
