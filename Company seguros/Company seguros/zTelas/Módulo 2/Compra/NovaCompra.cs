﻿using Company_seguros.DB.Cliente;
using Company_seguros.DB.Estoque;
using Company_seguros.DB.Funcionario;
using Company_seguros.DB.MODULO2.Compra;
using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.DB.MODULO2.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class NovaCompra : Form
    {
        BindingList<DTO_Produto> produtosCarrinho = new BindingList<DTO_Produto>();

        public NovaCompra()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();


        }

        void CarregarCombos()
        {
            Business_Produto business = new Business_Produto();
            List<DTO_Produto> lista = business.Listar();

            cboProduto.ValueMember = nameof(DTO_Produto.ID);
            cboProduto.DisplayMember = nameof(DTO_Produto.Produto);
            cboProduto.DataSource = lista;

            //Fornecedor
            Business_Fornecedor business_fornecedor = new Business_Fornecedor();
            List<DTO_Fornecedor> lista_fornecedor = business_fornecedor.Listar();

            cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);
            cboFornecedor.DataSource = lista_fornecedor;

            //Funcionario

            //Fornecedor
            Business_Funcionario business_funcionario = new Business_Funcionario();
            List<DTO_Funcionario> lista3 = business_funcionario.Listar();

            cboFuncionario.ValueMember = nameof(DTO_Funcionario.Id);
            cboFuncionario.DisplayMember = nameof(DTO_Funcionario.Funcionario);
            cboFuncionario.DataSource = lista3;

        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
       

        private void button1_Click(object sender, EventArgs e)
        {
           

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DTO_Produto dto = cboProduto.SelectedItem as DTO_Produto;
          
            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void NovaCompra_Load(object sender, EventArgs e)
        {

        }

        private void NovaCompra_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void voltarAoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            MENU frm = new MENU();
            frm.Show();
            Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCompra_Click(object sender, EventArgs e)
        {


            // salva na compra
            Business_Compra busi = new Business_Compra();
            // pegar id do produto

            DTO_Produto produto = cboProduto.SelectedItem as DTO_Produto;

            // pegar id do fornecedor
            DTO_Fornecedor fornecedor = cboFornecedor.SelectedItem as DTO_Fornecedor;

            //pegar id do funcionario
            DTO_Funcionario funcionario = cboFuncionario.SelectedItem as DTO_Funcionario;

            DTO_Compra dt = new DTO_Compra();


            dt.ID_Fornecedor = fornecedor.ID;
            dt.Data_Compra = DateTime.Now;
            dt.ID_Funcionario = funcionario.Id;

            busi.Salvar(dt, produtosCarrinho.ToList());


            // Estoque

            foreach (DTO_Produto item in produtosCarrinho)
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.IdProduto = item.ID;

                Business_Estoque b = new Business_Estoque();
                dto = b.Consultar(dto);

                dto.Quantidade = dto.Quantidade + 1;

                b.Alterar(dto);

            }

            MessageBox.Show("Compra bem sucedida");
        }
    }
}
