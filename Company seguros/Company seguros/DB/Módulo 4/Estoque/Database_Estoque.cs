﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Estoque
{
   public class Database_Estoque
    {
        Database db = new Database();

        public int Salvar(DTO_Estoque dto)
        {
            string script =
                @"INSERT INTO tb_estoque
                (
                    qtd_minima,
                    qtd_maxima,
                    qtd_atual,
                    id_produto
                )
                VALUES
                (
                    @qtd_minima,
                    @qtd_maxima,
                    @qtd_atual,
                    @id_produto
                )
                ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_minima", dto.QuantidadeMinima));
            parms.Add(new MySqlParameter("qtd_maxima", dto.QuantidadeMaxima));
            parms.Add(new MySqlParameter("qtd_atual", dto.Quantidade));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_Estoque dto)
        {
            string script =
                @"UPDATE tb_estoque SET qtd_minima = @qtd_minima,
                                        qtd_maxima = @qtd_maxima,
                                        qtd_atual = @qtd_atual,
                                        id_produto = @id_produto
                WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_minima", dto.QuantidadeMinima));
            parms.Add(new MySqlParameter("qtd_maxima", dto.QuantidadeMaxima));
            parms.Add(new MySqlParameter("qtd_atual", dto.Quantidade));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

             db.ExecuteInsertScript(script, parms);
        }

        public DTO_Estoque Consultar(DTO_Estoque dto)
        {
            string script =
                @"SELECT * FROM tb_estoque WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            DTO_Estoque dt = new DTO_Estoque();

            while(reader.Read())
            {
                dto.ID = reader.GetInt32("id_estoque");
                dto.IdProduto = reader.GetInt32("id_produto");
                dto.Quantidade = reader.GetInt32("qtd_atual");
                dto.QuantidadeMinima = reader.GetInt32("qtd_minima");
                dto.QuantidadeMaxima = reader.GetInt32("qtd_maxima");

                dt = dto;
            }
            return dt;
        }

        public void Remover(DTO_Estoque dto)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            db.ExecuteInsertScript(script, parms);
        }


    }
}
