﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Estoque
{
    public class DTO_Estoque
    {
        public int ID { get; set; }

        public int IdProduto { get; set; }

        public int Quantidade { get; set; }

        public int QuantidadeMinima { get; set; }

        public int QuantidadeMaxima { get; set; }


    }
}
