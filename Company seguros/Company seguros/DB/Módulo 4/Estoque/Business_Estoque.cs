﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Estoque
{
    public class Business_Estoque
    {
        Database_Estoque db = new Database_Estoque();

        public int Salvar(DTO_Estoque dto)
        {
            return db.Salvar(dto);
        }

        public void Alterar(DTO_Estoque dto)
        {
            db.Alterar(dto);
        }
        public DTO_Estoque Consultar(DTO_Estoque dto)
        {
            return db.Consultar(dto);
        }

        public void Remover(DTO_Estoque dto)
        {
            db.Remover(dto);
        }
    }
}
