﻿using Company_seguros.DB._base;
using Company_seguros.DB.Estoque;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_4.View
{
   public class View_Estoque
    {
        Database db = new Database();

        public List<DTO_EstoqueView> Consultar(DTO_EstoqueView dto)
        {
            string script =
                @"SELECT * FROM view_consultar_estoque WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + dto.Produto + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_EstoqueView> lista = new List<DTO_EstoqueView>();

            while (reader.Read())
            {
                DTO_EstoqueView dt = new DTO_EstoqueView();
                dt.Produto = reader.GetString("nm_produto");
                dt.Quantidade = reader.GetInt32("qtd_atual");
                dt.QuantidadeMaxima = reader.GetInt32("qtd_maxima");
                dt.QuantidadeMinima = reader.GetInt32("qtd_minima");
                lista.Add(dt);
            }
            return lista;
        }
    }
}
