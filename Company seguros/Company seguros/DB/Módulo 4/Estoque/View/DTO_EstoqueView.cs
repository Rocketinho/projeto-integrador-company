﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_4.View
{
    public class DTO_EstoqueView
    {
        public string Produto { get; set; }

        public int QuantidadeMinima { get; set; }

        public int QuantidadeMaxima { get; set; }

        public int Quantidade { get; set; }

    }
}
