﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_4.Fluxo_de_caixa
{
    public class DTO_ViewFolha
    {
        public DateTime Data { get; set; }

        public decimal TotalGanhos { get; set; }

        public decimal TotalDespesas { get; set; }

        public decimal Valor { get; set; }

    }
}
