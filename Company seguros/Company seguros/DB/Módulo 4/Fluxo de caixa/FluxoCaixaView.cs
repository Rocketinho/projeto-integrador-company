﻿using Company_seguros.DB._base;
using Company_seguros.DB.Módulo_4.Fluxo_de_caixa;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Fluxo_de_caixa
{
    public class FluxoCaixaView
    {
        public List<DTO_ViewFolha> Consultar(DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM vw_fluxodecaixa
                                      WHERE dt_referencia >= @comeco
                                        AND dt_referencia <= @fim";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("comeco", comeco));
            parm.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_ViewFolha> fora = new List<DTO_ViewFolha>();

            while (reader.Read())
            {
                DTO_ViewFolha dentro = new DTO_ViewFolha();
                dentro.Data = reader.GetDateTime("dt_referencia");
                dentro.TotalGanhos = reader.GetDecimal("vl_total_ganhos");
                dentro.TotalDespesas = reader.GetDecimal("vl_total_despesas");
                dentro.Valor = reader.GetDecimal("vl_saldo");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
