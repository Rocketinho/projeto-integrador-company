﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.FolhadePagamento
{
    public class DTO_FolhaPagamento
    {
        public int Id { get; set; }

        public decimal Horas_Trabalhadas { get; set; }

        public decimal Valor_Horas_Extras { get; set; }

        public decimal FGTS { get; set; }

        public decimal INSS { get; set; }

        public decimal IR { get; set; }

        public decimal Dependente { get; set; }

        public decimal Bruto { get; set; }

        public decimal Liquido { get; set; }

        public int Id_Funcionario { get; set; }



    }
}
