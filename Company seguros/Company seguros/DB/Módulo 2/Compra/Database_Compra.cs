﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Compra
{
    public class Database_Compra
    {
        public int Salvar(DTO_Compra dto)
        {
            string script =
            @"INSERT INTO tb_compra_item
            (
                id_fornecedor,
                id_produto,
                id_funcionario,
                dt_compra
              
            )
            VALUES
            (
                @id_fornecedor,
                @id_produto,
                @id_funcionario,
                @dt_compra
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("id_produto", dto.ID_Produto));
            parm.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parm.Add(new MySqlParameter("dt_compra", dto.Data_Compra));
           

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Remover(int dto)
        {
            string script = @"DELETE FROM tb_compra_item
                                    WHERE id_compra = @id_compra";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_compra", dto));

            Database db = new Database();

            db.ExecuteInsertScript(script, parm);
        }
        public void Alterar(DTO_Compra dto)
        {
            string script =
                @"UPDATE tb_compra_item
                     SET    id_fornecedor       =@id_fornecedor,
                            id_produto          =@id_produto,
                            id_funcionario      =@id_funcionario,
                            dt_compra           =@dt_compra
                           
                  WHERE     id_produto          = @id_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_compra", dto.ID));
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("id_produto", dto.ID_Produto));
            parm.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parm.Add(new MySqlParameter("dt_compra", dto.Data_Compra));
           
            Database db = new Database();

            db.ExecuteInsertScript(script, parm);
        }

        public List<DTO_Compra> Listar()
        {
            string script = "SELECT * FROM tb_compra_item";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Compra> fora = new List<DTO_Compra>();

            while (reader.Read())
            {
                DTO_Compra dentro = new DTO_Compra();
                dentro.ID = reader.GetInt32("id_compra");
                dentro.ID_Fornecedor = reader.GetInt32("id_fornecedor");
                dentro.ID_Funcionario = reader.GetInt32("id_funcionario");
                dentro.ID_Produto = reader.GetInt32("id_produto");
                dentro.Data_Compra = reader.GetDateTime("dt_compra");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

    }
}
