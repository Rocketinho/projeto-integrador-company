﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Compra
{
   public class DTO_Compra
    {
        public int ID { get; set; }
        public int ID_Fornecedor { get; set; }
        public int ID_Produto { get; set; }
        public int ID_Funcionario { get; set; }
        public string Lote { get; set; }
        public DateTime Validade { get; set; }
        public DateTime Data_Compra { get; set; }
    }
}
