﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_2.Compra.View
{
    public class ViewCompra
    {
        Database db = new Database();
        public List<DTO_ViewCompra> Consultar(DTO_ViewCompra dto)
        {
            string script =
                @"SELECT * FROM view_consultar_compra WHERE ds_razao_social like @ds_razao_social";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_razao_social", "%" + dto.Razao + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_ViewCompra> lista = new List<DTO_ViewCompra>();
            
            while(reader.Read())
            {
                DTO_ViewCompra dt = new DTO_ViewCompra();
                dt.Razao = reader.GetString("ds_razao_social");
                dt.Funcionario = reader.GetString("nm_funcionario");
                dt.Data = reader.GetDateTime("dt_compra");
                dt.Quantidade = reader.GetInt32("qtd_produto");
                dt.Total = reader.GetDecimal("vl_total");
                lista.Add(dt);

            }
            return lista;
        }
    }
}
