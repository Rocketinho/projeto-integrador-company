﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_2.Compra.View
{
    public class DTO_ViewCompra
    {
        public string Razao { get; set; }

        public string Funcionario { get; set; }

        public DateTime Data { get; set; }

        public int Quantidade { get; set; }

        public decimal Total { get; set; }
    }
}
