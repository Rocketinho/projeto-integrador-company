﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Fornecedor
{
    public class Business_Fornecedor
    {
        //Método de escopo
        Database_Fornecedor db = new Database_Fornecedor();

        //Método de Salvar
        public int Salvar(DTO_Fornecedor dto)
        {
            return db.Salvar(dto);
        }

        //Método de Remover
        public void Remover(int id)
        {
            db.Remover(id);
        }

        //Método de Alterar
        public void Alterar(DTO_Fornecedor dto)
        {
            db.Alterar(dto);
        }

        //Método de Listar (Sem filtro)
        public List<DTO_Fornecedor> Listar()
        {
            List<DTO_Fornecedor> list = db.Listar();
            return list;
        }

        //Método de Consultar (Com filtro)
        public List<DTO_Fornecedor> Consultar(DTO_Fornecedor dto)
        {
            List<DTO_Fornecedor> list = db.Consultar(dto);
            return list;
        }
        
    }
}
