﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Produto
{
    public class DTO_Produto
    {
        public int ID { get; set; }

        public string Fornecedor { get; set; }

        public string Produto { get; set; }

        public Decimal Preco { get; set; }

        public string Marca { get; set; }

        public string Imagem { get; set; }
    }
}
