﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Produto
{
    public class Database_Produto
    {
        //Método de escopo
       Database db = new Database();

        //Método de Salvar
        public int Salvar(DTO_Produto dto)
        {
            string script =
            @"INSERT INTO tb_produto
            (
                
                nm_produto,
                vl_preco,
                nm_marca
            )
            VALUES
            (
               
               @nm_produto,
               @vl_preco,
               @nm_marca
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
          
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("nm_marca", dto.Marca));

            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        //Método de Remover
        public void Remover(int dto)
        {
            string script = @"DELETE FROM tb_produto
                                    WHERE id_produto = @id_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto", dto));

            db.ExecuteInsertScript(script, parm);
        }

        //Método de Alterar
        public void Alterar(DTO_Produto dto)
        {
            string script =
                @"UPDATE tb_produto
                     SET    
                            nm_produto      =@nm_produto,
                            vl_preco        =@vl_preco,
                            nm_marca        =@nm_marca
                  WHERE     id_produto      =@id_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_produto", dto.ID));
  
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));
            parm.Add(new MySqlParameter("nm_marca", dto.Marca));

            db.ExecuteInsertScript(script, parm);
        }

        //Método de listar (Sem filtro). Este método trás todos os registros da tabela.
        public List<DTO_Produto> Listar()
        {
            string script = "SELECT * FROM tb_produto";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Produto> fora = new List<DTO_Produto>();

            while (reader.Read())
            {
                DTO_Produto dentro = new DTO_Produto();
                dentro.ID = reader.GetInt32("id_produto");
      
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Marca = reader.GetString("nm_marca");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        //Método de consultar (Com filtro). Este método trás todos os valores correspondentre a um determinado produto
        public List<DTO_Produto> Consultar(DTO_Produto dto)
        {
            string script = @"SELECT * FROM tb_produto
                                      WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", "%" + dto.Produto + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Produto> fora = new List<DTO_Produto>();

            while (reader.Read())
            {
                DTO_Produto dentro = new DTO_Produto();
                dentro.ID = reader.GetInt32("id_produto");
              
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Marca = reader.GetString("nm_marca");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

      
    }
}
