﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda.View
{
   public class DTO_View
    {
        public string Cliente { get; set; }

        public string Funcionario { get; set; }

        public string RG { get; set; }

        public DateTime DataVenda { get; set; }

        public int Quantidade { get; set; }

        public decimal Total { get; set; }

    }
}
