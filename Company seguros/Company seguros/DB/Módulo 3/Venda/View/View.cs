﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda.View
{
    public class View  
    {
        Database database = new Database();

        public List<DTO_View> Consultar(DTO_View dto)
        {
            string script =
                @"SELECT * FROM view_consultar_venda WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + dto.Cliente + "%"));
            MySqlDataReader reader = database.ExecuteSelectScript(script, parms);

            List<DTO_View> lista = new List<DTO_View>();
            while(reader.Read())
            {
                DTO_View dt = new DTO_View();
                dt.Cliente = reader.GetString("nm_cliente");
                dt.Funcionario = reader.GetString("nm_funcionario");
                dt.RG = reader.GetString("ds_RG");
                dt.DataVenda = reader.GetDateTime("dt_venda");
                dt.Quantidade = reader.GetInt32("qtd_produto");
                dt.Total = reader.GetDecimal("vl_produtos");
                lista.Add(dt);

            }
            reader.Close();
            return lista;
        }
    }
}
