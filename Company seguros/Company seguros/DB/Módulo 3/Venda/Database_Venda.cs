﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda
{
    public class Database_Venda
    {
        _base.Database db = new _base.Database();

        public int Salvar(DTO_Venda dto)
        {
            string script =
                @"INSERT INTO tb_venda
                (
                id_funcionario,
                id_cliente,
                id_produto_venda,
                dt_venda,
                id_produto
                )

                VALUES
                (
                @id_funcionario,
                @id_cliente,
                @id_produto_venda,
                @dt_venda,
                @id_produto
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            parms.Add(new MySqlParameter("id_cliente", dto.IdCliente));
            parms.Add(new MySqlParameter("id_produto_venda", dto.IdServico));
            parms.Add(new MySqlParameter("dt_venda", dto.DataVenda));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));


            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Venda dto)
        {
            string script =
               @"UPDATE tb_venda SET id_funcionario = @id_funcionario,
                                     id_cliente = @id_cliente,
                                     id_produto_venda = @id_produto_venda,
                                     dt_venda = @dt_venda,
                                     id_produto = @id_produto
                WHERE id_venda = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", dto.ID));
            parms.Add(new MySqlParameter("id_funcionario", dto.IdFuncionario));
            parms.Add(new MySqlParameter("id_cliente", dto.IdCliente));
            parms.Add(new MySqlParameter("id_produto_venda", dto.IdServico));
            parms.Add(new MySqlParameter("dt_venda", dto.DataVenda));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(DTO_Venda dto)
        {
            string script =
               @"DELETE * FROM tb_venda WHERE id_venda = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", dto.ID));

            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Venda> Listar()
        {
            string script =
                @"SELECT * FROM tb_venda";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_Venda> lista = new List<DTO_Venda>();
            while (reader.Read())
            {
                DTO_Venda dt = new DTO_Venda();
                dt.ID = reader.GetInt32("id_venda");
                dt.IdFuncionario = reader.GetInt32("id_funcionario");
                dt.IdCliente = reader.GetInt32("id_cliente");
                dt.IdServico = reader.GetInt32("id_produto_venda");
                dt.DataVenda = reader.GetDateTime("dt_venda");
                dt.IdProduto = reader.GetInt32("id_produto");


                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }


        public List<DTO_Venda> Consultar(DTO_Venda dto)
        {
            string script =
                @"SELECT * FROM tb_venda WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.IdCliente));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Venda> lista = new List<DTO_Venda>();
            while (reader.Read())
            {
                DTO_Venda dt = new DTO_Venda();
                dt.ID = reader.GetInt32("id_venda");
                dt.IdFuncionario = reader.GetInt32("id_funcionario");
                dt.IdCliente = reader.GetInt32("id_cliente");
                dt.IdServico = reader.GetInt32("id_produto_venda");
                dt.DataVenda = reader.GetDateTime("dt_venda");
                dt.IdProduto = reader.GetInt32("id_produto");

                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
    }
}
