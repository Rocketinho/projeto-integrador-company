﻿using Company_seguros.DB.Cliente;
using Company_seguros.DB.Funcionario;
using Company_seguros.DB.MODULO2.Produto;
using Company_seguros.DB.ProdutoVenda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda
{
    public class Business_Venda
    {
        Database_Venda db = new Database_Venda();

        public void Salvar(List<DTO_Produto> lista, DTO_Cliente dtoCliente, DTO_Funcionario dtoFuncionario, DTO_ProdutoVenda produtoProduto)
        {
            DateTime date = DateTime.Now;
            foreach (DTO_Produto item in lista)
            {
                DTO_Venda dto = new DTO_Venda();
                dto.IdProduto = item.ID;
                dto.IdCliente = dtoCliente.Id;
                dto.DataVenda = date;
                dto.IdFuncionario = dtoFuncionario.Id;
                dto.IdServico = produtoProduto.ID;
                db.Salvar(dto);

            }
        }
        public void Alterar(DTO_Venda dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Venda dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Venda> Listar()
        {
            return db.Listar();
        }
        public List<DTO_Venda> Consultar(DTO_Venda dto)
        {
           return db.Consultar(dto);
        }
    }
}
