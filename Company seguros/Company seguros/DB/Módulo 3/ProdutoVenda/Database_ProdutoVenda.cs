﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.ProdutoVenda
{
    public class Database_ProdutoVenda
    {
        _base.Database db = new _base.Database();

        public int Salvar(DTO_ProdutoVenda dto)
        {
            string script =
                @"INSERT INTO tb_produto_venda
                (
                ds_servico,
                vl_instalacao
                )

                VALUES
                (
                @ds_servico,
                @vl_instalacao
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_servico", dto.Servico));
            parms.Add(new MySqlParameter("vl_instalacao", dto.Precoinstalacao));


            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        
        public void Alterar(DTO_ProdutoVenda dto)
        {
            string script =
               @"UPDATE tb_produto_venda SET ds_servico = @ds_servico,
                                             vl_instalacao = @vl_instalacao
                                            
                WHERE id_produto_venda = @id_produto_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto_venda", dto.ID));
            parms.Add(new MySqlParameter("ds_servico", dto.Servico));
            parms.Add(new MySqlParameter("vl_instalacao", dto.Precoinstalacao));



            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(DTO_ProdutoVenda dto)
        {
            string script =
               @"DELETE FROM tb_produto_venda WHERE id_produto_venda = @id_produto_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto_venda", dto.ID));
            

            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_ProdutoVenda> Listar()
        {
            string script =
                @"SELECT * FROM tb_produto_venda";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_ProdutoVenda> lista = new List<DTO_ProdutoVenda>();
            while(reader.Read())
            {
                DTO_ProdutoVenda dt = new DTO_ProdutoVenda();
                dt.ID = reader.GetInt32("id_produto_venda");
                dt.Servico = reader.GetString("ds_servico");
                dt.Precoinstalacao = reader.GetDecimal("vl_instalacao");
               
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_ProdutoVenda> Consultar(DTO_ProdutoVenda dto)
        {
            string script =
                @"SELECT * FROM tb_produto_venda WHERE ds_servico like @ds_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();          
            parms.Add(new MySqlParameter("ds_servico", "%" + dto.Servico + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_ProdutoVenda> lista = new List<DTO_ProdutoVenda>();
            while (reader.Read())
            {
                DTO_ProdutoVenda dt = new DTO_ProdutoVenda();
                dt.ID = reader.GetInt32("id_produto_venda");
                dt.Servico = reader.GetString("ds_servico");
                dt.Precoinstalacao = reader.GetDecimal("vl_instalacao");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
    }
}
