﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.ProdutoVenda
{
   public class Business_ProdutoVenda
    {
        Database_ProdutoVenda db = new Database_ProdutoVenda();
        
        public int Salvar(DTO_ProdutoVenda dto)
        {
            return db.Salvar(dto);
        }
        public void Alterar(DTO_ProdutoVenda dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_ProdutoVenda dto)
        {
            db.Remover(dto);
        }
        public List<DTO_ProdutoVenda> Listar()
        {
            return db.Listar();
        }
        public List<DTO_ProdutoVenda> Consultar(DTO_ProdutoVenda dto)
        {
            return db.Consultar(dto);
        }
    }
}
