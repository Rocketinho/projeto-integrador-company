﻿using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Company_seguros.DB.Funcionario.VIEW_Acesso;

namespace Company_seguros
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        void Logar()
        {
            VIEW_Acesso_Funcionario dto = new VIEW_Acesso_Funcionario();
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;

            VIEW_Acesso_Funcionario db = new VIEW_Acesso_Funcionario();
            VIEW_Acesso_Funcionario user = db.Logar(dto);

            if (user != null)
            {
                UserSession.Logado = user;
                this.Hide();
                MENU tela = new MENU();
                tela.Show();
            }
            else
            {
                MessageBox.Show("Credênciais inválidas!!",
                                "Company Seguros",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Logar();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void lblSenha_Click(object sender, EventArgs e)
        {

        }

        private void lblUsuario_Click(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click_1(object sender, EventArgs e)
        {
            Logar();

        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            if (txtSenha.Text == "Password")
            {
                txtSenha.isPassword = true;
                txtSenha.Text = "";
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            if (txtSenha.Text == "")
            {
                txtSenha.isPassword = false;
                txtSenha.Text = "Password";
                txtSenha.ForeColor = Color.Silver;
            }
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Logar();
            }
        }
    }
}
